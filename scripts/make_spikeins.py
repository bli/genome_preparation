#!/usr/bin/env python3
# coding: utf-8
"""
Script to download, format and write spike-in sequences and annotations.
"""

import argparse
import sys
from zipfile import ZipFile
import tempfile
import shutil
from io import BytesIO
from pathlib import Path
import requests
from yaml import safe_load as yload



def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # Where files will be generated
    parser.add_argument(
        "-d", "--transgenes_dir",
        help="Path to the directory in which sequences and annotation files "
        "will be written.")
    parser.add_argument(
        "-c", "--transgene_config_file",
        help="Path to a yaml-formatted file containing "
        "the transgene description.")
    # parser.add_argument(
    #     "-g", "--genome",
    #     default=example_genome,
    #     help="Genome name, that will be used in the output file names.")
    # parser.add_argument(
    #     "-b", "--biotype",
    #     default=example_biotype,
    #     help="What biotype to use for the transgene?")
    args = parser.parse_args()
    outdir = Path(args.transgenes_dir)
    outdir.mkdir(parents=True, exist_ok=True)
    with open(args.transgene_config_file) as config_fh:
        config = yload(config_fh)
        spike_data_url = config.get(
            "spike_data_url",
            "https://assets.thermofisher.com/TFS-Assets/LSG/manuals/ERCC92.zip")
        spikeins_name = config.get("name", "spike_ins")
        spikeins_biotype = config.get("biotype", "spike_ins")
    with tempfile.TemporaryDirectory() as tmpdir:
        req = requests.get(spike_data_url)
        zipfile = ZipFile(BytesIO(req.content))
        zipfile.extractall(tmpdir)
        for path in Path(tmpdir).iterdir():
            if path.suffix == ".fa":
                # TODO: Figure out why the make_transgene_info rule needs the fasta file
                # to be named like that (with "transcript")?
                shutil.copy(path, outdir.joinpath(f"{spikeins_name}_transcript.fa"))
            if path.suffix == ".gtf":
                # Python 3.10 new syntax, not allowed here
                # with (
                #         open(path) as gtf_in,
                #         open(outdir.joinpath(f"{spikeins_name}.gtf"), "w") as gtf_out):
                with open(path) as gtf_in, \
                        open(outdir.joinpath(f"{spikeins_name}.gtf"), "w") as gtf_out:
                    for line in gtf_in:
                        fields = line.strip().split("\t")
                        fields[2] = "transcript"
                        fields[-1] += f" gene_biotype \"{spikeins_biotype}\";"
                        gtf_out.write("\t".join(fields) + "\n")
    return 0


if __name__ == "__main__":
    sys.exit(main())
