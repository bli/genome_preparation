#!/usr/bin/env python3
"""
"""

import argparse
import sys
from collections import OrderedDict
from io import StringIO
from re import compile
from pathlib import Path
from mappy import fastx_read
# from Bio import SeqIO
# https://docs.pyfilesystem.org/en/latest/guide.html
from fs.osfs import OSFS as FS

def get_record_names(fasta):
    """Extract record names from fasta-formatted string *fasta*."""
    for line in fasta.split("\n"):
        if line.startswith(">"):
            yield line[1:].split()[0]


# Transgene from Rechavi Cell 2020 (04/09/2020)
example_fasta_transgene = """\
>mex5_promoter
ATCTGCAAGAAAATACATTTTCGACTGATTTTACGGTTTTCACAACGGCAAAATATCAGTTTTTAAAAAATTAAACCATAAAACAAATAATATAACCCAATTTTTACATCAAACCACAAGAAAAAAATACATTTGGGCCCACGGATAAAGAAATTAAAAAAATACATTTTTTAAAGGCGCACCGAATTAAAATTCATTTGGGTCTTACCGCGTATACCGTACTCCGTTTGTTTGATCATTTTTGTCAGCGCTGGCGGTTGTTTTTTCATTTCATTTCTGCTTCAAAGACGTTTTCTCGAATAATTTTTCGTTTATTCTCTTTTTTAAAATTAATTTCTAGCCGTAAATGTTATAAATTCACCCATTTAACGCAAATTTCATGGTAATCTCATGGAAAAATGCAGTTTCTTTGTTAAAGAAAGCTTAAATAGCAAAAATTCCCCGACTTTCCCCAAAATCCTGCTCGATTTTCCGTTTTCTCATTGTATTCTCTCTTAATTAATTTTATCGATAATCAATTGAATGTTTCAGACAGAGA
>exon_1
ATGGTCTCAAAGGGTGAAGAAGATAACATGGCAATTATTAAAGAGTTTATGCGTTTCAAGGTGCATATGGAGGGATCTGTCAATGGGCATGAGTTTGAAATTGAAGGTGAAGGAGAAGGCCGACCATATGAGGGAACACAAACCGCAAAACTAAAG
>intron_1
GTAAGTTTAAACATATATATACTAACTAACCCTGATTATTTAAATTTTCAG
>exon_2
GTAACTAAAGGCGGACCATTACCATTCGCCTGGGACATCCTCTCTCCACAGTTCATGTATGGAAGTAAAGCTTATGTTAAACATCCGGCAGATATACCAGATTATTTGAAACTTTCATTCCCGGAGGGTTTTAAGTGGGAACGCGTAATGAATTTTGAAGACGGAGGAGTTGTTACAGTGACGCAAGACTCAAG
>intron_2
GTAAGTTTAAACAGTTCGGTACTAACTAACCATACATATTTAAATTTTCAG
>exon_3
CCTCCAAGATGGAGAATTTATTTATAAAGTCAAACTTCGAGGAACGAATTTCCCCTCGGATGGACCTGTTATGCAGAAGAAGACTATGGGATGGGAAGCTTCAAGTGAAAGAATGTACCCTGAAGACGGTGCTCTTAAGGGAGAGATTAAACAACGTCTTAAATTGAAAGATGGAGGACATTACGATGCTGAG
>intron_3
GTAAGTTTAAACATGATTTTACTAACTAACTAATCTGATTTAAATTTTCAG
>exon_4
GTGAAGACAACTTACAAAGCCAAAAAACCAGTTCAGCTGCCAGGAGCGTACAATGTTAATATTAAACTGGATATCACCTCCCACAACGAGGATTACACTATCGTTGAGCAATATGAAAGAGCTGAAGGGCGGCACTCGACAGGTGGCATGGATGAATTGTATAA
>his58
ATGCCACCAAAGCCATCTGCCAAGGGAGCCAAGAAGGCCGCCAAGACCGTCGTTGCCAAGCCAAAGGACGGAAAGAAGAGACGTCATGCCCGCAAGGAATCGTACTCCGTCTACATCTACCGTGTTCTCAAGCAAGTTCACCCAGACACCGGAGTCTCCTCCAAGGCCATGTCTATCATGAACTCCTTCGTCAACGATGTATTCGAACGCATCGCTTCGGAAGCTTCCCGTCTTGCTCATTACAACAAACGCTCAACGATCTCATCCCGCGAAATTCAAACCGCTGTCCGTTTGATTCTCCCAGGAGAACTTGCCAAGCACGCCGTGTCTGAGGGAACCAAGGCCGTCACCAAGTACACTTCCAGCAAGTAA
>tbb2_3UTR
ATGCAAGATCCTTTCAAGCATTCCCTTCTTCTCTATCACTCTTCTTTCTTTTTGTCAAAAAATTCTCTCGCTAATTTATTTGCTTTTTTAATGTTATTATTTTATGACTTTTTATAGTCACTGAAAAGTTTGCATCTGAGTGAAGTGAATGCTATCAAAATGTGATTCTGTCTGATGTACTTTCACAATCTCTCTTCAATTCCATTTTGAAGTGCTTTAAACCCGAAAGGTTGAGAAAAATGCGAGCGCTCAAATATTTGTATTGTGTTCGTTGAGTGACCCAACAAAAAGAGGAAA
"""

example_transcript_parts = ["exon_1", "intron_1", "exon_2", "intron_2", "exon_3", "intron_3", "exon_4"]

example_biotype = "protein_coding"

example_genome = "mCherry_Rechavi"

example_pseudo_chrom = "mCherry"

example_gene_id = "mCherry"

example_transcript_id = example_gene_id


def main():
    """Main function of the program."""
    # TODO: Use argparse to provide the fasta, parts and genome name as arguments
    fasta_transgene = example_fasta_transgene
    # TODO: generate default based on sequence names in fasta_transgene
    # (those following a exon_* or intron_* pattern)
    transcript_re = compile("(exon|intron)_\d+")
    transcript_parts = [
        record_name
        for record_name in get_record_names(fasta_transgene)
        if transcript_re.match(record_name)]
    # transcript_parts = example_transcript_parts
    genome = example_genome
    pseudo_chrom = example_pseudo_chrom
    gene_id = example_gene_id
    transcript_id = example_transcript_id
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # Where files will be generated
    parser.add_argument(
        "-d", "--transgenes_dir",
        help="Path to the directory in which sequences and annotation files "
        "will be written.")
    parser.add_argument(
        "-g", "--genome",
        default=example_genome,
        help="Genome name, that will be used in the output file names.")
    parser.add_argument(
        "-b", "--biotype",
        default=example_biotype,
        help="What biotype to use for the transgene?")
    args = parser.parse_args()
    Path(args.transgenes_dir).mkdir(parents=True, exist_ok=True)
    with FS(args.transgenes_dir) as fs:
        # Writing the transgene structure
        ##################################
        with fs.open(f"{args.genome}_transgene_parts.fa", "w") as transgene_file:
            transgene_file.write(fasta_transgene)
            transgene_filename =transgene_file.name.decode("utf-8")
        print(f"{transgene_filename} written")
        sequence_parts = OrderedDict((seq_name, seq) for (seq_name, seq, _) in fastx_read(transgene_filename))
        # Making a sequence for the transcript only
        ############################################
        transcript_seq = "".join(sequence_parts[seq_name] for seq_name in transcript_parts)
        with fs.open(f"{genome}_transcript.fa", "w") as transcript_file:
            # Should we use gene_id or transcript_id?
            # transcript_file.write(f">{gene_id}\n{transcript_seq}\n")
            transcript_file.write(f">{transcript_id}\n{transcript_seq}\n")
        print(f"{transcript_file.name.decode('utf-8')} written")
        # Making a gtf annotation for the transcript
        #############################################
        with fs.open(f"{genome}.gtf", "w") as transcript_gtf_file:
            annotations = f"gene_id \"{gene_id}\"; transcript_id \"{transcript_id}\"; gene_biotype \"{args.biotype}\";"
            transcript_gtf_file.write("\t".join([
                f"{pseudo_chrom}", "local", "transcript", "1", str(len(transcript_seq)),
                "0", "+", ".", annotations]))
            transcript_gtf_file.write("\n")
            # Not used, and difficult to make more generic
            # with fs.open(f"{genome}_transgene.gtf", "w") as transgene_gtf_file:
            #     last_pos_in_transgene = 0
            #     last_pos_in_transcript = 0
            #     exon_number = 0
            #     for (seq_name, seq) in sequence_parts.items():
            #         start_in_transgene = last_pos_in_transgene + 1
            #         end_in_transgene = last_pos_in_transgene + len(seq)
            #         if seq_name == "mex5_promoter":
            #             annotations = "gene_id \"mex5\"; transcript_id \"mex5\"; gene_biotype \"transgene\";"
            #             transgene_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
            #                 "0", "+", ".", annotations]))
            #             transgene_gtf_file.write("\n")
            #         elif seq_name.startswith("exon"):
            #             start_in_transcript = last_pos_in_transcript + 1
            #             end_in_transcript = last_pos_in_transcript + len(seq)
            #             exon_number += 1
            #             annotations = f"exon_id \"mCherry_Rechavi.e{exon_number}\"; exon_number \"{exon_number}\"; gene_id \"mCherry_Rechavi\"; transcript_id \"mCherry_Rechavi\"; gene_biotype \"transgene\";"
            #             transgene_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "exon", str(start_in_transgene), str(end_in_transgene),
            #                 "0", "+", ".", annotations]))
            #             transgene_gtf_file.write("\n")
            #             annotations = f"exon_id \"mCherry_Rechavi.e{exon_number}\"; exon_number \"{exon_number}\"; gene_id \"mCherry_Rechavi\"; transcript_id \"mCherry_Rechavi\"; gene_biotype \"protein_coding\";"
            #             transcript_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "exon", str(start_in_transcript), str(end_in_transcript),
            #                 "0", "+", ".", annotations]))
            #             transcript_gtf_file.write("\n")
            #             last_pos_in_transcript = end_in_transcript
            #         elif seq_name.startswith("intron"):
            #             start_in_transcript = last_pos_in_transcript + 1
            #             end_in_transcript = last_pos_in_transcript + len(seq)
            #             annotations = "gene_id \"mCherry_Rechavi\"; transcript_id \"mCherry_Rechavi\"; gene_biotype \"transgene\";"
            #             transgene_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "intron", str(start_in_transgene), str(end_in_transgene),
            #                 "0", "+", ".", annotations]))
            #             transgene_gtf_file.write("\n")
            #             last_pos_in_transcript = end_in_transcript
            #         elif seq_name == "his58":
            #             annotations = "gene_id \"his58\"; transcript_id \"his58\"; gene_biotype \"transgene\";"
            #             transgene_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "transcript", str(start_in_transgene), str(end_in_transgene),
            #                 "0", "+", ".", annotations]))
            #             transgene_gtf_file.write("\n")
            #         elif seq_name == "tbb2_3UTR":
            #             annotations = "gene_id \"tbb2\"; transcript_id \"tbb2\"; gene_biotype \"transgene\";"
            #             transgene_gtf_file.write("\t".join([
            #                 f"{genome}_transgene", "local", "UTR", str(start_in_transgene), str(end_in_transgene),
            #                 "0", "+", ".", annotations]))
            #             transgene_gtf_file.write("\n")
            #         else:
            #             raise ValueError(f"{seq_name} does not belong to the transgene.")
            #         last_pos_in_transgene = end_in_transgene
            # print(f"{transgene_gtf_file.name.decode('utf-8')} written")
        print(f"{transcript_gtf_file.name.decode('utf-8')} written")
    return 0


if __name__ == "__main__":
    sys.exit(main())
