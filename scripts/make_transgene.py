#!/usr/bin/env python3
"""
"""

import argparse
import sys
from collections import OrderedDict
from re import compile
from pathlib import Path
# from mappy import fastx_read
# https://docs.pyfilesystem.org/en/latest/guide.html
from fs.osfs import OSFS as FS
from yaml import safe_load as yload
# To be 100% sequence element order is preserved
# from oyaml import safe_load as yload

# def get_record_names(fasta):
#     """Extract record names from fasta-formatted string *fasta*."""
#     for line in fasta.split("\n"):
#         if line.startswith(">"):
#             yield line[1:].split()[0]


example_fasta_transgene = """\
>mex5_promoter
ATCTGCAAGAAAATACATTTTCGACTGATTTTACGGTTTTCACAACGGCAAAATATCAGTTTTTAAAAAATTAAACCATAAAACAAATAATATAACCCAATTTTTACATCAAACCACAAGAAAAAAATACATTTGGGCCCACGGATAAAGAAATTAAAAAAATACATTTTTTAAAGGCGCACCGAATTAAAATTCATTTGGGTCTTACCGCGTATACCGTACTCCGTTTGTTTGATCATTTTTGTCAGCGCTGGCGGTTGTTTTTTCATTTCATTTCTGCTTCAAAGACGTTTTCTCGAATAATTTTTCGTTTATTCTCTTTTTTAAAATTAATTTCTAGCCGTAAATGTTATAAATTCACCCATTTAACGCAAATTTCATGGTAATCTCATGGAAAAATGCAGTTTCTTTGTTAAAGAAAGCTTAAATAGCAAAAATTCCCCGACTTTCCCCAAAATCCTGCTCGATTTTCCGTTTTCTCATTGTATTCTCTCTTAATTAATTTTATCGATAATCAATTGAATGTTTCAGACAGAGA
>exon_1
ATGGGAGGTAGGGCCGGCTCTGTCTCAAAGGGTGAAGAAGATAACATGGCAATTATTAAAGAGTTTATGCGTTTCAAGGTGCATATGGAGGGATCTGTCAATGGGCATGAGTTTGAAATTGAAGGTGAAGGAGAAGGCCGACCATATGAGGGAACACAAACCGCAAAACTAAAG
>intron_1
GTAAGTTTAAACATATATATACTAACTAACCCTGATTATTTAAATTTTCAG
>exon_2
GTAACTAAAGGCGGACCATTACCATTCGCCTGGGACATCCTCTCTCCACAGTTCATGTATGGAAGTAAAGCTTATGTTAAACATCCGGCAGATATACCAGATTATTTGAAACTTTCATTCCCGGAGGGTTTTAAGTGGGAACGCGTAATGAATTTTGAAGACGGAGGAGTTGTTACAGTGACGCAAGACTCAAG
>intron_2
GTAAGTTTAAACAGTTCGGTACTAACTAACCATACATATTTAAATTTTCAG
>exon_3
CCTCCAAGATGGAGAATTTATTTATAAAGTCAAACTTCGAGGAACGAATTTCCCCTCGGATGGACCTGTTATGCAGAAGAAGACTATGGGATGGGAAGCTTCAAGTGAAAGAATGTACCCTGAAGACGGTGCTCTTAAGGGAGAGATTAAACAACGTCTTAAATTGAAAGATGGAGGACATTACGATGCTGAG
>intron_3
GTAAGTTTAAACATGATTTTACTAACTAACTAATCTGATTTAAATTTTCAG
>exon_4
GTGAAGACAACTTACAAAGCCAAAAAACCAGTTCAGCTGCCAGGAGCGTACAATGTTAATATTAAACTGGATATCACCTCCCACAACGAGGATTACACTATCGTTGAGCAATATGAAAGAGCTGAAGGGCGGCACTCGACAGGTGGCATGGATGAATTGTATAAGGGAGGAGGAGGAGGTGGA
>his11
ATGCCACCAAAGCCATCTGCCAAGGGAGCCAAGAAGGCCGCCAAGACCGTTACGAAGCCAAAGGACGGAAAGAAGAGACGTCATGCCCGTAAGGAATCATACTCCGTCTACATCTACCGTGTCCTCAAGCAAGTTCATCCAGACACTGGAGTTTCCTCCAAAGCCATGTCTATCATGAACTCTTTTGTCAACGATGTCTTCGAGCGTATTGCTGCTGAAGCATCCCGTCTTGCTCACTACAACAAGCGTTCCACAATCTCATCCCGCGAAATTCAGACCGCTGTCCGTCTGATCCTTCCAGGAGAGCTTGCCAAGCACGCCGTGTCTGAGGGAACCAAGGCCGTTACCAAGTACACTTCCAGCAAGTAA
>tbb2_3UTR
ATGCAAGATCCTTTCAAGCATTCCCTTCTTCTCTATCACTCTTCTTTCTTTTTGTCAAAAAATTCTCTCGCTAATTTATTTGCTTTTTTAATGTTATTATTTTATGACTTTTTATAGTCACTGAAAAGTTTGCATCTGAGTGAAGTGAATGCTATCAAAATGTGATTCTGTCTGATGTACTTTCACAATCTCTCTTCAATTCCATTTTGAAGTGCTTTAAACCCGAAAGGTTGAGAAAAATGCGAGCGCTCAAATATTTGTATTGTGTTCGTTGAGTGACCCAACAAAAAGAGGAAA
"""

example_transcript_parts = ["exon_1", "intron_1", "exon_2", "intron_2", "exon_3", "intron_3", "exon_4"]

example_biotype = "protein_coding"

example_genome = "mCherry"

example_pseudo_chrom = example_genome

example_gene_id = example_genome

example_transcript_id = example_gene_id

transcript_re = compile("(exon|intron)_(\d+)")

class Transgene:
    def __init__(self, config_file):
        with open(config_file) as fh:
            config = yload(fh)
        self.name = config["name"]
        self.biotype = config["biotype"]
        self.pseudo_chrom = config.get("pseudo_chrom", self.name)
        self.gene_id = config.get("gene_id", self.name)
        self.transcript_id = config.get("transcript_id", self.gene_id)
        self.sequence = OrderedDict(config["sequence"])
        default_transcript_parts = []
        # Last encountered intron or exon number
        exin_num = 0
        for part_name in self.sequence:
            m = transcript_re.match(part_name)
            if m:
                (_, num) = m.groups()
                if int(num) < exin_num:
                    raise ValueError(
                        "Exon and intron numbering doesn't seem to be increasing.")
                default_transcript_parts.append(part_name)
                exin_num = int(num)
        # default_transcript_parts = [
        #     part_name
        #     for part_name in self.sequence
        #     if transcript_re.match(part_name)]
        self.transcript_parts = config.get(
            "transcript_parts", default_transcript_parts)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # Where files will be generated
    parser.add_argument(
        "-d", "--transgenes_dir",
        help="Path to the directory in which sequences and annotation files "
        "will be written.")
    parser.add_argument(
        "-c", "--transgene_config_file",
        help="Path to a yaml-formatted file containing "
        "the transgene description.")
    # parser.add_argument(
    #     "-g", "--genome",
    #     default=example_genome,
    #     help="Genome name, that will be used in the output file names.")
    # parser.add_argument(
    #     "-b", "--biotype",
    #     default=example_biotype,
    #     help="What biotype to use for the transgene?")
    args = parser.parse_args()
    Path(args.transgenes_dir).mkdir(parents=True, exist_ok=True)
    transgene = Transgene(args.transgene_config_file)
    with FS(args.transgenes_dir) as fs:
        # TODO: make code part of Transgene methods
        # Writing the transgene structure
        ##################################
        with fs.open(f"{transgene.name}_transgene_parts.fa", "w") as transgene_fh:
            for (seq_name, seq) in transgene.sequence.items():
                transgene_fh.write(f">{seq_name}\n{seq}\n")
            transgene_filename = transgene_fh.name.decode("utf-8")
        print(f"{transgene_filename} written")
        # sequence_parts = OrderedDict(
        #     (seq_name, seq)
        #     for (seq_name, seq, _)
        #     in fastx_read(transgene_filename))
        # Making a sequence for the transcript only
        ############################################
        # Note that, depending on transcript.transcript_parts,
        # this may connect parts that are not actually contiguous in the genome.
        transcript_seq = "".join(
            transgene.sequence[seq_name]
            for seq_name in transgene.transcript_parts)
        with fs.open(f"{transgene.name}_transcript.fa", "w") as transcript_fh:
            # Should we use gene_id or transcript_id?
            # transcript_fh.write(f">{transgene.gene_id}\n{transcript_seq}\n")
            transcript_fh.write(f">{transgene.transcript_id}\n{transcript_seq}\n")
        print(f"{transcript_fh.name.decode('utf-8')} written")
        # Making a gtf annotation for the transcript
        #############################################
        with fs.open(f"{transgene.name}.gtf", "w") as transcript_gtf_file:
            annotations = f"gene_id \"{transgene.gene_id}\"; transcript_id \"{transgene.transcript_id}\"; gene_biotype \"{transgene.biotype}\";"
            transcript_gtf_file.write("\t".join([
                f"{transgene.pseudo_chrom}", "local", "transcript", "1", str(len(transcript_seq)),
                "0", "+", ".", annotations]))
            transcript_gtf_file.write("\n")
            print(f"{transcript_gtf_file.name.decode('utf-8')} written")
        print(f"{transcript_gtf_file.name.decode('utf-8')} written")
    return 0


if __name__ == "__main__":
    sys.exit(main())
