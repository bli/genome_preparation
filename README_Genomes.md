Getting C. elegans data
=======================

    mkdir -p /Genomes/C_elegans

File `C_elegans/21U-RNA_observed.gff` is provided by G. Cecere. Contains 5623
21U-RNA observed by Bartel lab. Unsure about source.


UCSC data
---------

    # We actually didn't use this
    #cd /Genomes/C_elegans
    #wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Caenorhabditis_elegans/UCSC/ce10/Caenorhabditis_elegans_UCSC_ce10.tar.gz
    #tar -xvzf Caenorhabditis_elegans_UCSC_ce10.tar.gz
    #rm -f Caenorhabditis_elegans_UCSC_ce10.tar.gz


```
grep -c C_elegans/Caenorhabditis_elegans/UCSC/ce10/Annotation/Archives/archive-current/Genes/genes.gtf
# 13511
```

### Getting transposon annotations

    cd /Genomes/C_elegans
    mkdir -p repeats
    cd repeats
    wget http://hgdownload.cse.ucsc.edu/goldenPath/ce11/database/rmsk.txt.gz
    gunzip rmsk.txt.gz


```
bioawk -t '{hist[$12]++} END {for (t in hist) print t,hist[t]}' rmsk.txt 
#Low_complexity	6346
#LINE	654
#Simple_repeat	22616
#rRNA	6
#RC	4551
#ARTEFACT	2
#LTR	673
#DNA?	755
#Satellite	5444
#DNA	33579
#SINE	531
#Unknown	9070
```

    # Set 3rd column in gtf as "transcript", for simplicity
    #bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "DNA" || $12 == "DNA?" || $12 == "Unknown" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_id "$12"|"$13"|"$11":"count[$11]"; transcript_id "$12"|"$13"|"$11":"count[$11]";"}' rmsk.txt \
    #    | sed 's/^chr//' > transposable_elements.gtf
    bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "DNA" || $12 == "DNA?" || $12 == "Unknown" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \"transposable_elements_rmsk\"; gene_id \""$12"|"$13"|"$11":"count[$11]"\"; transcript_id \""$12"|"$13"|"$11":"count[$11]"\";"}' rmsk.txt \
        | sed 's/^chr//' > transposable_elements.gtf
    bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "DNA" || $12 == "DNA?" || $12 == "Unknown" {count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}' \
        rmsk.txt | sed 's/^chr//' > transposable_elements.bed
    # Examine length distribution of the annotated genomic fragments
    #bioawk -t '{hist[$3-$2]++} END {for (l in hist) print l,hist[l]}' \
    #    transposable_elements.bed | sort -n \
    #    > transposable_elements_lengths_histogram.txt


Generate annotation files for these 4 categories
------------------------------------------------

### DNA transposons

In this category, we include "RC", "DNA?" and "DNA".

    #bioawk -t '$12 == "RC" || $12 == "DNA" || $12 == "DNA?" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_id "$12"|"$13"|"$11":"count[$11]"; transcript_id "$12"|"$13"|"$11":"count[$11]";"}' rmsk.txt \
    #    | sed 's/^chr//' > DNA_transposons.gtf
    bioawk -t '$12 == "RC" || $12 == "DNA" || $12 == "DNA?" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \"DNA_transposons_rmsk\"; gene_id \""$12"|"$13"|"$11":"count[$11]"\"; transcript_id \""$12"|"$13"|"$11":"count[$11]"\";"}' rmsk.txt \
        | sed 's/^chr//' > DNA_transposons.gtf
    bioawk -t '$12 == "RC" || $12 == "DNA" || $12 == "DNA?" {count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}' \
        rmsk.txt | sed 's/^chr//' > DNA_transposons.bed

### RNA transposons

In this category, we include "LINE", "LTR" and "SINE"

    #bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "SINE" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_id "$12"|"$13"|"$11":"count[$11]"; transcript_id "$12"|"$13"|"$11":"count[$11]";"}' rmsk.txt \
    #    | sed 's/^chr//' > RNA_transposons.gtf
    bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "SINE" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \"RNA_transposons_rmsk\"; gene_id \""$12"|"$13"|"$11":"count[$11]"\"; transcript_id \""$12"|"$13"|"$11":"count[$11]"\";"}' rmsk.txt \
        | sed 's/^chr//' > RNA_transposons.gtf
    bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "SINE" {count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}' \
        rmsk.txt | sed 's/^chr//' > RNA_transposons.bed

### Satellites

    #bioawk -t '$12 == "Satellite" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_id "$12"|"$13"|"$11":"count[$11]"; transcript_id "$12"|"$13"|"$11":"count[$11]";"}' rmsk.txt \
    #    | sed 's/^chr//' > satellites.gtf
    bioawk -t '$12 == "Satellite" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \"satellites_rmsk\"; gene_id \""$12"|"$13"|"$11":"count[$11]"\"; transcript_id \""$12"|"$13"|"$11":"count[$11]"\";"}' rmsk.txt \
        | sed 's/^chr//' > satellites.gtf
    bioawk -t '$12 == "Satellite" {count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}' \
        rmsk.txt | sed 's/^chr//' > satellites.bed

### Simple repeats

    #bioawk -t '$12 == "Simple_repeat" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_id "$12"|"$13"|"$11":"count[$11]"; transcript_id "$12"|"$13"|"$11":"count[$11]";"}' rmsk.txt \
    #    | sed 's/^chr//' > simple_repeats.gtf
    bioawk -t '$12 == "Simple_repeat" {count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \"simple_repeats_rmsk\"; gene_id \""$12"|"$13"|"$11":"count[$11]"\"; transcript_id \""$12"|"$13"|"$11":"count[$11]"\";"}' rmsk.txt \
        | sed 's/^chr//' > simple_repeats.gtf
    bioawk -t '$12 == "Simple_repeat" {count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}' \
        rmsk.txt | sed 's/^chr//' > simple_repeats.bed




    wget ftp://ftp.wormbase.org/pub/wormbase/releases/WS253/species/c_elegans/PRJNA13758/c_elegans.PRJNA13758.WS253.transposon_transcripts.fa.gz
    zcat c_elegans.PRJNA13758.WS253.transposon_transcripts.fa.gz \
        | grep "^>" | awk '{print $3}' | awk -F "=" '{print $2}' \
        > transposable_elements_ids.txt
    zcat c_elegans.PRJNA13758.WS253.canonical_geneset.gtf.gz \
        | bioawk -t '{print $9}' | awk '{print $2}' | sed 's/.*"\(.*\)".*/\1/' \
        > canonical_geneset_ids.txt
    # Data generated in october 2016 by Paul Davis <paul.davis@wormbase.org>:
    wget ftp://ftp.sanger.ac.uk/pub2/wormbase/STAFF/pad/blaise/Transposon.gff



Ensembl data
------------


    cd /Genomes/C_elegans
    wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Caenorhabditis_elegans/Ensembl/WBcel235/Caenorhabditis_elegans_Ensembl_WBcel235.tar.gz
    tar -xvzf Caenorhabditis_elegans_Ensembl_WBcel235.tar.gz
    rm -f Caenorhabditis_elegans_Ensembl_WBcel235.tar.gz
    ln -s genes.gtf Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_all.gtf


### Generating 10-bp bins along the genome

This is useful to create binned bedgraph files.

    cd /Genomes/C_elegans
    mawk '$1 == "@SQ" {print $2"\t0\t"$3}' Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.dict \
        | sed 's/SN://g' | sed 's/LN://' \
        | bedops --chop 10 - \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed

### Generating separate gtf files for each annotation type

```
extract_info_from_gtf.py Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf 
#Storing id2name in Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_id2name.pickle
#antisense	103	(max length = 5885)
#pseudogene	1612	(max length = 17899)
#snRNA	130	(max length = 187)
#lincRNA	176	(max length = 6035)
#tRNA	637	(max length = 122)
#piRNA	15365	(max length = 21)
#protein_coding	31293	(max length = 134155)
#rRNA	22	(max length = 3509)
#miRNA	451	(max length = 26)
#snoRNA	345	(max length = 344)
#ncRNA	7700	(max length = 8360)
```

    cd /Genomes/C_elegans
    for type in antisense pseudogene snRNA lincRNA tRNA piRNA protein_coding rRNA miRNA snoRNA ncRNA all
    do
        # bedops gtf2bed seems to have parsing problems
        #gtf2bed < Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_${type}.gtf > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_${type}.bed
        ensembl_gtf2bed.py Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_${type}.gtf | uniq > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_${type}.bed
        ln -s "genes_${type}.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/${type}.gtf
        ln -s "genes_${type}.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/${type}.bed
    done
    ln -s "../../../../../../../repeats/transposable_elements.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/transposable_elements_rmsk.gtf
    ln -s "../../../../../../../repeats/transposable_elements.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/transposable_elements_rmsk.bed
    ln -s "../../../../../../../repeats/DNA_transposons.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/DNA_transposons_rmsk.gtf
    ln -s "../../../../../../../repeats/DNA_transposons.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/DNA_transposons_rmsk.bed
    ln -s "../../../../../../../repeats/RNA_transposons.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/RNA_transposons_rmsk.gtf
    ln -s "../../../../../../../repeats/RNA_transposons.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/RNA_transposons_rmsk.bed
    ln -s "../../../../../../../repeats/satellites.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/satellites_rmsk.gtf
    ln -s "../../../../../../../repeats/satellites.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/satellites_rmsk.bed
    ln -s "../../../../../../../repeats/simple_repeats.gtf" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/simple_repeats_rmsk.gtf
    ln -s "../../../../../../../repeats/simple_repeats.bed" Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/simple_repeats_rmsk.bed
    # Note 25/04/2018: The output directory doesn't seem to exist, commenting
    # extract_transcripts_from_gtf.py -g Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf -o Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Transcripts
    compute_genes_exon_lengths.py --annot_dir "Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/"

### Merging transcripts

    # gff_merge_transcripts.sh uses gtf2attr, bioawk and bedtools
    cd /Genomes/C_elegans
    for type in antisense pseudogene snRNA lincRNA tRNA piRNA protein_coding rRNA miRNA snoRNA ncRNA all DNA_transposons_rmsk RNA_transposons_rmsk satellites_rmsk simple_repeats_rmsk
    do
        gff_merge_transcripts.sh Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/${type}.gtf \
            > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/${type}_merged.bed
    done

$ head -6 /tmp/bed_with_gene_ids.bed | bedtools groupby -g 1,4,5,6 -c 2,3 -o min,max | awk -v OFS="\t" '{print $1,$5,$6,$2,$3,$4}'
I	3746	3909	"WBGene00023193"	.	-
I	4118	10230	"WBGene00022277"	.	-
$ head -6 /tmp/bed_with_gene_ids.bed | bedtools groupby -g 1,4,6,5 -c 2,3 -o min,max -i "stdin" | mawk -v OFS="\t" '{print $1,$5,$6,$2,$3,$4}'
I	3746	3909	"WBGene00023193"	-	.
I	4118	10230	"WBGene00022277"	-	.

### Correcting mitochondrial chromosome name

    cd /Genomes/C_elegans
    sed -i 's/^M/MtDNA/' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/*rmsk*

### Extracting exons and generating "pure introns"

We assume that "CDS" and "UTR" together form the protein-coding gene exon annotations.

We can check that the only `gene_biotype` gtf attribute in such annotations is `protein_coding`:
```
awk '$3 == "CDS" || $3 == "UTR" {print}' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf | sed 's/^.*gene_biotype "//' | sed 's/".*$//' | uniq 
#protein_coding
```

Note: We use "transcript" as gtf feature (column 3) in order to simplify the
counting by featureCounts, but this results in the annotations not displaying
correctly in IGV, probably because several "transcripts" end up having the same
`transcript_id`.

    cd /Genomes/C_elegans
    #bioawk '$3 == "CDS" {print}' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS.gtf
    #awk '$3 == "UTR" {print}' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_UTR.gtf
    ## Merge the two to get the positions where protein coding exons are present
    #sort -k1,1 -k4,4n -m Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS.gtf Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_UTR.gtf > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS_UTR.gtf
    ## Substract the exons from the transcripts to get the positions were only introns are present
    #bedtools subtract \
    #    -a Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding.gtf \
    #    -b Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS_UTR.gtf \
    #    | bioawk -t '$3="pure_intron" {print}' > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_pure_intron.gtf
    bioawk -t '$3 == "CDS" {$3="transcript"; print}' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf \
        | sed 's/gene_biotype \"protein_coding\"/gene_biotype \"protein_coding_CDS\"/' \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS.gtf 
    bioawk -t '$3 == "UTR" {$3="transcript"; print}' Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf \
        | sed 's/gene_biotype \"protein_coding\"/gene_biotype \"protein_coding_UTR\"/' \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_UTR.gtf
    # Merge the two to get the positions where protein coding exons are present
    sort -k1,1 -k4,4n -m \
        Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS.gtf \
        Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_UTR.gtf \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS_UTR.gtf
    # Substract the exons from the transcripts to get the positions were only introns are present
    bedtools subtract \
        -a Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding.gtf \
        -b Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_CDS_UTR.gtf \
        | sed 's/gene_biotype \"protein_coding\"/gene_biotype \"protein_coding_pure_intron\"/' \
        | sort -k1,1 -k4,4n > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/protein_coding_pure_intron.gtf

### Differentiating UTRs

    orient_gtf_UTRs.py --annot_dir /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes

### Extract histone genes

    cd /Genomes/C_elegans
    extract_histone_genes_from_gtf.py \
        Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf \
        | tee Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_histone.gtf \
        | ensembl_gtf2bed.py \
        | merge_bed.py \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_histone.bed
    cd /Genomes/C_elegans
    # Needs bedtools 2.26 (or above?)
    bedtools getfasta -name -fullHeader -s \
        -fi Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa \
        -bed Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_histone.bed \
        | tee Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/histone.fa \
        | sed 's/^>\(.*\)::/>\t\1\t\1::/' | id2name.py | sed 's/^>\t/>/' \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/histone_converted.fa

    mkdir -p bowtie2
    bowtie2-build --seed 123 --packed Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/piRNA_converted.fa bowtie2/piRNA
    

[his-57](http://www.wormbase.org/species/c_elegans/gene/WBGene00001931#0-9g-3)
and
[his-72](http://www.wormbase.org/species/c_elegans/gene/WBGene00001946#0-9g-3)
have several isoforms.
`merge_bed.py` is used to merge the coordinates of the isoforms so that the
resulting bed entry spans all isoforms.

### Extract piRNA genes

    bedtools getfasta -name -fullHeader -s \
        -fi Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa \
        -bed Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_piRNA.bed \
        | tee Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/piRNA.fa \
        | sed 's/^>\(.*\)::/>\t\1\t\1::/' | id2name.py | sed 's/^>\t/>/' \
        > Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/piRNA_converted.fa




### Obtaining conversion information from BioMart

```perl
# An example script demonstrating the use of BioMart API.
# This perl API representation is only available for configuration versions >=  0.5 
use strict;
use BioMart::Initializer;
use BioMart::Query;
use BioMart::QueryRunner;

my $confFile = "PATH TO YOUR REGISTRY FILE UNDER biomart-perl/conf/. For Biomart Central Registry navigate to
						http://www.biomart.org/biomart/martservice?type=registry";
#
# NB: change action to 'clean' if you wish to start a fresh configuration  
# and to 'cached' if you want to skip configuration step on subsequent runs from the same registry
#

my $action='cached';
my $initializer = BioMart::Initializer->new('registryFile'=>$confFile, 'action'=>$action);
my $registry = $initializer->getRegistry;

my $query = BioMart::Query->new('registry'=>$registry,'virtualSchemaName'=>'default');

		
	$query->setDataset("celegans_gene_ensembl");
	$query->addAttribute("ensembl_gene_id");
	$query->addAttribute("embl");
	$query->addAttribute("wormbase_gene");
	$query->addAttribute("ensembl_transcript_id");
	$query->addAttribute("external_gene_name");
	$query->addAttribute("external_transcript_name");
	$query->addAttribute("ensembl_peptide_id");
	$query->addAttribute("gene_biotype");
	$query->addAttribute("transcript_biotype");
	$query->addAttribute("start_position");
	$query->addAttribute("end_position");
	$query->addAttribute("strand");
	$query->addAttribute("wormbase_transcript");

$query->formatter("TSV");

my $query_runner = BioMart::QueryRunner->new();
############################## GET COUNT ############################
# $query->count(1);
# $query_runner->execute($query);
# print $query_runner->getCount();
#####################################################################


############################## GET RESULTS ##########################
# to obtain unique rows only
# $query_runner->uniqueRowsOnly(1);

$query_runner->execute($query);
$query_runner->printHeader();
$query_runner->printResults();
$query_runner->printFooter();
#####################################################################
```

Downloaded from the web interface and saved in `/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/Ensembl_ID_conversions.txt`

This actually contains lot of redundant identifiers, but not the gene name <-> cosmid name correspondance.


Data from WormBase
------------------

Some explanations from <http://www.wormbase.org/about/userguide/nomenclature#aebfli9mc04g216738dkj5h--10>:

> Genetic Loci
> 
>     A Gene is a region that is expressed or a region that has been expressed and is now a Pseudogene.
>     A gene can be a Pseudogene, or can express one or more non-coding RNA genes (ncRNA) or protein-coding sequences (CDS).
>     All WormBase genes have a unique identifier like WBGene00006415.
>         This is guaranteed to consistently follow the gene throughout any changes that may be made to its structure.
>         When gene structures are split into two genes, the original gene ID will usually apply to the 5' gene and a new gene ID will be created for the other half. 
>     All C. elegans WormBase genes also have a Sequence Name, which is derived from the cosmid, fosmid or YAC clone on which they reside, for instance F38H4.7, indicating it is on the cosmid F38H4, and there are at least 6 other genes on that cosmid. 
> 
> Approved gene names
> 
>     If a gene produces a protein that can be classified as a member of a family, the gene may also be assigned a Approved name consisting of three or four italicized letters, a hyphen, and an italicized Arabic number, e.g., unc-30 indicating that this is the 30th member of the unc gene family.
>     There are a few exceptions to this format, like the genes cln-3.1, cln-3.2, and cln-3.3 which all are equally similar to the human gene CLN3.
>     Gene GCG names for non-elegans species in WormBase have the 3-letter species code prepended, like Cre-acl-5, Cbr-acl-5, Cbn-acl-5.
>     The gene name may on rare occasions be followed by an italicized Roman numeral, to indicate the linkage group on which the gene maps, e.g., dpy-5 I or let-37 X or mlc-3 III.
>     Assignment of gene family names is controlled by WormBase and requests for names should be made, before publication, via the form. or via email to: genenames@wormbase.org
>     For genes defined by mutation, the Approved gene names refer to the mutant phenotype originally detected or most easily scored e.g.
>         dumpy (dumpy) in the case of dpy-5
>         lethal (lethal) in the case of let-37. 
>     For genes defined on the basis of sequence similarity or sequence features, the Approved gene name refers to the predicted protein product or RNA product e.g.
>         myosin light chain in the case of mlc-3,
>         superoxide dismutase in the case of sod-1,
>         NPHP (human kidney disease nephronophthisis gene) in the case of nph-4.
>         ribosomal RNA in the case of rrn-1. 
>     Genes with related properties are usually given the same three-letter name and different numbers. For example, there are three known myosin light chain genes: mlc-1, mlc-2, mlc-3, and more than twenty different dumpy genes: dpy-1, dpy-2, dpy-3, and so on.
>     Genes can be given names corresponding to homologous named genes in other standard genetic organisms. e.g.
>         rnt-1 is the C. elegans ortholog of the Drosophila gene runt.
>         wrn-1 is the C. elegans ortholog of the human gene WRN1, responsible for Werner's syndrome. 
>     Gene names that are memorable, informative and simply explained are encouraged.
>     Genes in a paralogous set related to a single named gene in another organism are sometimes given the same gene name and number, followed by a distinguishing decimal. e.g. four C. elegans genes homologous to SIR2 in S. cerevisiae have been given the names sir-2.1, sir-2.2, sir-2.3, sir-2.4.
>     Gene names based solely on RNAi phenotypes or high-throughput analysis of gene expression or protein interaction are discouraged.
>     Gene names including c (for Caenorhabditis), ce (for C. elegans), n (for nematode) or w (for worm) are discouraged. Instead, an optional prefix Cel- can be added to indicate the species origin.
>     A limited number of genes have been given temporary tag- names (tag = temporarily assigned gene name). These are genes for which deletion alleles have been generated by reverse genetic methods, but which have not yet been given more informative names based on sequence or mutant phenotype. When sufficient information becomes available, each tag name will be replaced by an appropriate standard 3-letter or 4-letter name.
>     A limited number of genes, named on the basis of sequence homology, have been given non-standard names ending with alphanumeric identifiers rather than with simple numbers, in order to make these names closer to the generally accepted names used in other organisms. e.g. eif-3.B, eif-3.C encode proteins of the conserved translation factor eIF3. 
> 
> Approved Gene Name Conflicts
> 
> Approved Gene names that have been established in the published literature and databases should preferably not be changed. In cases where a gene has received multiple names, one name will be adopted as the main name for the gene. Other names will continue to be listed in databases. Whenever possible, name changes or the adoption of a single main name should be made with the approval of all laboratories concerned.
> Homologous Genes
> 
> If a homolog of a known C. elegans gene is identified in a related species such as Caenorhabditis briggsae, this can be given the same gene name, preceded by three italic letters referring to the species, and a hyphen. For example, Cbr-tra-1 is the name for the C. briggsae homolog of the C. elegans gene tra-1. The C. elegans homolog of a gene identified and named in another organism can be distinguished by the same convention, using "Cel-" as an optional prefix. For example, Cel-snt-1 defines the C. elegans synaptotagmin gene.
> Alleles and Mutations
> 
>     Every mutation has a unique designation. Mutations are given names consisting of one or two italicized letters followed by an italicized Arabic number, e.g., e61 or mn138 or st5. The letter prefix refers to the laboratory of isolation, as registered with the CGC. There are currently more than 500 registered laboratories. For example, e refers (originally) to the MRC Laboratory of Molecular Biology (Cambridge, U.K.), (currently) to the laboratory of J. Hodgkin (University of Oxford), and st refers to the laboratory of R.H. Waterston (originally at Washington University, St. Louis, MO, currently at the University of Washington, Seattle).
>     When gene and mutation names are used together, the mutation name is included in parentheses after the gene name, e.g., dpy-5(e61), let-37(mn138). When unambiguous (e.g., if only one mutation is known for a given gene or if all work on a gene described in a publication used a single mutation cited in a Methods section), gene names are used in preference to mutation names (let-37 rather than mn138 or let-37(mn138)).
>     Optional suffixes indicating characteristics of a mutation can follow a mutation name. These are usually two-letter nonitalicized letters, e.g., hc17ts, where ts stands for temperature-sensitive, or pk15te, where te stands for transposon-excision.
>     Mutations created by in vitro mutagenesis should receive standard allele names. For cases where a pre-existing genomic mutation is re-created by in vitro mutagenesis, it is still desirable to give the new mutation a new name.
>     The wild-type allele of a gene is defined as that present in the Bristol N2 strain, stored frozen at the CGC and other locations. Wild-type alleles can be designated by a plus sign immediately after the gene name, dpy-5+, or, more commonly, by including the plus sign in parentheses, dpy-5(+). 
> 
> Gene Knockouts
> 
> Most gene knockouts constructed to date are small deletions (<5 kb) generated by transposon excision or by chemical mutagenesis. These are named as alleles, sometimes with the optional suffix te (transposon-excision) or ko (knockout). Example: zyx-1(gk190) is a 777 bp deletion in the zyx-1 gene.
> 
> Some knockouts have been made by insertion of a selectable marker, such as unc-119(+). These are named as alleles, with an optional descriptor defining the selected marker following the unique allele name, and preceded by a double colon. Example: jf61 = zhp-3(jf61::unc-119+)
> 
> Some of the small deletions generated by reverse genetic methods may remove parts of two adjacent genes. If only two genes appear to be affected, then the deletion is given a single allele name, but the genotype is written with both gene names coupled with an ampersand (&). Example: allele ok615 is a 1422 bp deletion of two adjacent genes, so it can be written rad-54&tag-157(ok615).
> 
> Deletions that affect more than two genes are named as Deficiencies ''(Df)'', as described in the Chromosomal Aberrations section.
> 
> Modifers: Suppressors, Revertants and Enhancers
> 
> There is no special nomenclature for modifier mutations. Many extragenic suppressor loci are called sup (40 sup loci defined so far, with a wide variety of properties and mechanisms). An increasing number of more specific modifier gene classes have been established, such as smu (suppressor of mec and unc), and smg (suppressor with morphogenetic effect on genitalia) and sel (suppressor/enhancer of lin-12).
> 
> Intragenic suppressors or modifiers are indicated by adding a second mutation name within parentheses; for example, unc-17(e245e2608) is an intragenic partial revertant of unc-17(e245).
> 
> Mutations known to be chromosomal rearrangements, rather than intragenic lesions, are named differently, as described in the Chromosomal Aberrations section.
> 
> Chromosomal Aberrations
> 
> Duplications (Dp) deficiencies (Df), inversions (In) and translocations (T) are known in C. elegans cytogenetics; these are given italicized names consisting of the laboratory mutation prefix, the relevant abbreviation, and a number, optionally followed by the affected linkage groups in parentheses (e.g., eT1(III;V), mnDp5(X;f), where f indicates a free duplication). Chromosomal balancers of unknown structure can be designated using the abbreviation C, e.g., mnC1(II).



    cd /Genomes/C_elegans
    # WS253 is the "production" realease as of 02/08/2016
    mkdir -p Wormbase/WS253
    cd Wormbase/WS253
    # This file contains conversion info for gene identifiers
    wget ftp://ftp.wormbase.org/pub/wormbase/releases/WS253/species/c_elegans/PRJNA13758/c_elegans.PRJNA13758.WS253.xrefs.txt.gz
    gunzip c_elegans.PRJNA13758.WS253.xrefs.txt.gz

After some header lines starting with "//", this file contains 9 tab-separated columns.

The header is the following:

```
//
// WormBase Caenorhabditis elegans XREFs for WS253
//
// Columns (tab separated) are:
//    1. WormBase Gene sequence name
//    2. WormBase Gene accession
//    3. WormBase Gene CGC name
//    4. WormBase Transcript sequence name
//    5. WormPep protein accession
//    6. INSDC parent sequence accession
//    7. INSDC locus_tag id
//    8. INSDC protein_id
//    9. UniProt accession
//
// Missing or not applicable data (e.g. protein identifiers for non-coding RNAs) is denoted by a "."
//
```

Column 1 corresponds to the cosmid-derived name mentioned here:

> All C. elegans WormBase genes also have a Sequence Name, which is derived
> from the cosmid, fosmid or YAC clone on which they reside, for instance
> F38H4.7, indicating it is on the cosmid F38H4, and there are at least 6 other
> genes on that cosmid.

Column 2 corresponds to the unique identifiers described here:

>     All WormBase genes have a unique identifier like WBGene00006415.
>         This is guaranteed to consistently follow the gene throughout any
>         changes that may be made to its structure. When gene structures are
>         split into two genes, the original gene ID will usually apply to the
>         5' gene and a new gene ID will be created for the other half. 


Column 3 corresponds to the approved gene names described here: 

>     If a gene produces a protein that can be classified as a member of a
>     family, the gene may also be assigned a Approved name consisting of three
>     or four italicized letters, a hyphen, and an italicized Arabic number,
>     e.g., unc-30 indicating that this is the 30th member of the unc gene
>     family.
>     There are a few exceptions to this format, like the genes cln-3.1,
>     cln-3.2, and cln-3.3 which all are equally similar to the human gene
>     CLN3.


### Generate conversion dictionaries

    grep -v "^//" c_elegans.PRJNA13758.WS253.xrefs.txt | bioawk -t '{print $1,$2,$3}' | uniq > cosmid_ID_name.txt
    make_gene_id_conversion_dicts.py cosmid_ID_name.txt

These pickled python dictionaries can be used on tab-separated data with
`${HOME}/src/RNA_Seq_Cecere/idconverter.py`, it's wrapper
`${HOME}/src/RNA_Seq_Cecere/convertids.sh` and the `wormid2name` `name2wormid`
`name2cosmid` `cosmid2name` `cosmid2wormid` `wormid2cosmid` aliases.


Worth noting:

<http://www.wormbase.org/species/c_elegans/gene/WBGene00206384#0-9g-3> has the following curatorial remark:

> acly-1 was assigned to this gene in error. acly-1 is D1005.1 (WBGene00016995)


    cd /Genomes/C_elegans/Wormbase/WS253
    mkdir geneIDs
    cd geneIDs
    wget ftp://ftp.wormbase.org/pub/wormbase/species/c_elegans/annotation/geneIDs/c_elegans.PRJNA13758.WS253.geneIDs.txt.gz
    gunzip c_elegans.PRJNA13758.WS253.geneIDs.txt.gz
    wget ftp://ftp.wormbase.org/pub/wormbase/species/c_elegans/annotation/geneOtherIDs/c_elegans.PRJNA13758.WS253.geneOtherIDs.txt.gz
    gunzip c_elegans.PRJNA13758.WS253.geneOtherIDs.txt.gz
    awk -F "," '{print $4"\t"$2"\t"$3}' c_elegans.PRJNA13758.WS253.geneIDs.txt | sed 's/^\t/\.\t/' | sed 's/\t$/\t\./' | uniq > cosmid_ID_name.txt
    make_gene_id_conversion_dicts.py cosmid_ID_name.txt
    wget http://tazendra.caltech.edu/~azurebrd/var/work/users/sourceFile/WBGeneName.csv



Other wormbase annotations
--------------------------

    cd /Genomes/C_elegans
    # WS253 is the "production" realease as of 02/08/2016
    mkdir -p Wormbase/WS253
    cd Wormbase/WS253
    # These annotation contain transposable elements
    wget ftp://ftp.wormbase.org/pub/wormbase/releases/WS253/species/c_elegans/PRJNA13758/c_elegans.PRJNA13758.WS253.annotations.gff2.gz

```
zcat c_elegans.PRJNA13758.WS253.annotations.gff2.gz | bioawk -t '{hist[$2]++} END {for (t in hist) print t,hist[t]}'
	10
dmelanogaster_proteins-BLASTX	159393
celegans_proteins-BLASTX	327102
promoter	94
tandem	53024
rRNA_Pseudogene	2
cbriggsae_proteins-BLASTX	277549
snRNA	260
scerevisiae_proteins-BLASTX	52144
TranscriptionallyActiveRegion	4095280
UTRome	21347
BLAT_ncRNA_BEST	5974
TF_binding_site_region	883791
ppacificus_proteins-BLASTX	178872
Promoterome	6851
PCoF_CGH_allele_Polymorphism	225
dust	189148
RNASeq_stranded	1442255
regulatory_region	69
Allele	6388
miRNA_mature	902
CGH_allele	68
tRNA_Pseudogene	422
binding_site	1586
jigsaw	369621
PCoF_Million_mutation	188289
translated_feature	550427
ovolvulus_proteins-BLASTX	140137
miRanda	71796
RNASEQ.Hillier	4623126
binding_site_region	683
RNASeq_R_asymmetry	474067
enhancer	2427
tRNA	1326
assembly_tag	10209
RNASEQ.Hillier.Aggregate	1027977
RNASeq	95458
Substitition_allele	6
gene	46761
EMBL_nematode_cDNAs-BLAT	1688928
miRNA_primary_transcript	16
deprecated_operon	110
BLAT_Trinity_BEST	159043
cisRed	7929
TEC_RED	7993
PCoF_Variation_project_Polymorphism	81851
BLAT_Caen_mRNA_OTHER	5300
histone_binding_site_region	5164
Vancouver_fosmid	12874
rRNA	44
polyA_signal_sequence	2454
RNASeq_F_asymmetry	489079
RNAi_primary	258490
cbrenneri_proteins-BLASTX	307809
pre_miRNA	512
inverted	65691
BLAT_ncRNA_OTHER	572
interpolated_pmap_position	904
transposable_element_gene	350
polyA_site	87271
BLAT_Trinity_OTHER	173089
BLAT_TC1_BEST	1043
segmental_duplication	3484
PCoF_KO_consortium	3834
Polysome_profiling	227070
landmark	116
PCoF_WGS_Rose	3345
BLAT_OST_BEST	106450
BLAT_Caen_Trinity_OTHER	458426
GeneMarkHMM	289950
PCoF_NBP_knockout	5607
BLAT_RST_OTHER	4971
piRNA	30730
Polymorphism	5
Transposon_Pseudogene	116
BLAT_Caen_EST_OTHER	1532421
curated	558711
lincRNA	684
KO_consortium	6254
BLAT_Caen_mRNA_BEST	6119
ncRNA	15684
non_coding_transcript	8010
NEMBASE_cDNAs-BLAT	1127090
scRNA	2
operon	1388
Expr_profile	17360
BLAT_TC1_OTHER	3579
Genomic_canonical	3267
Clone	8790
PCoF_CGH_allele	67
NemaGENETAG_consortium	15298
BLAT_EST_BEST	1178643
RNASeq_splice	475497
mGene	402150
BLAT_Caen_EST_BEST	329588
WGS_Rose	15193
Genbank	3267
mSplicer_orf	363752
pmid18538569	2907
PCoF_Substitition_allele	5
TF_binding_site	513
Oligo_set	514660
PCoF_Variation_project	2415
RNAi_secondary	77401
SAGE_tag_most_three_prime	13
Mos_insertion_allele	16
Transposon	757
absolute_pmap_position	25997
BLAT_EST_OTHER	501778
cDNA_for_RNAi	2669
cremanei_proteins-BLASTX	317405
BLAT_Caen_Trinity_BEST	187079
RNASeq_reads	581900
BLAT_mRNA_BEST	21793
Link	7
GenePair_STS	54350
NBP_knockout	6741
CGH_allele_Polymorphism	250
Transposon_CDS	2679
Variation_project_Polymorphism	897294
Coding_transcript	738546
DNAseI_hypersensitive_site	7095
Genefinder	401688
RNAz	3672
SAGE_tag	168646
mass_spec_genome	144134
snoRNA	690
bmalayi_proteins-BLASTX	175206
history	336135
Orfeome	22176
SAGE_tag_genomic_unique	121804
SL1	43207
SL2	13445
Chronogram	1974
RepeatMasker	116933
twinscan	372220
sratti_proteins-BLASTX	188744
Expr_pattern	2934
TSS_region	73499
PCoF_Allele	5129
Million_mutation	863882
mSplicer_transcript	369958
Variation_project	21644
UniProt-BLASTX	308920
BLAT_OST_OTHER	100146
asRNA	370
BLAT_mRNA_OTHER	6402
cjaponica_proteins-BLASTX	253765
hsapiens_proteins-BLASTX	324196
SAGE_tag_unambiguously_mapped	2511
BLAT_RST_BEST	7367
Pseudogene	13265
NEMATODE.NET_cDNAs-BLAT	1179018
```
```
zcat c_elegans.PRJNA13758.WS253.annotations.gff2.gz | bioawk -t '{hist[$3]++} END {for (t in hist) print t,hist[t]}'
	10
CLone	2
polymorphism	1
coding_exon	3389657
PCR_product	83377
unknown	6
Finished	2498
snRNA	130
repeat	385
transcript_region	2987301
possible_base_call_error	1233
nc_primary_transcript	593
final	3
Finisher	10
binding_site	81994
intron	2739762
histone_binding_site	5164
nucleotide_match	25489
DSTM	7
ignore	17
mRNA_region	227070
protein_coding_primary_transcript	33096
enhancer	2427
Comment	25
tRNA	634
gene	74128
deprecated_operon	110
miRNA_primary_transcript	8
cosmid	111
experimental_result_region	20029
three_prime_UTR	49149
motif_segment	385951
rRNA	22
complex_substitution	51180
inverted_repeat	65693
Clone_left_end	4916
SNP	729257
polyA_signal_sequence	2454
Finished_right	11
tandem_repeat	53024
RNAi_reagent	335891
transcribed_fragment	4095280
oligo	194
pre_miRNA	256
deletion	147115
sequencing	1
translated_nucleotide_match	144134
polyA_site	87271
tandem_duplication	4024
Finished_left	10
segmental_duplication	3484
Clone_right_end	3910
compression	65
Consensus	8
low_complexity	1
piRNA	15365
five_prime_UTR	31246
Polymorphism	16
repeat_region	116933
Sequence	1
lincRNA	177
insertion_site	90358
G-quartet	2907
ncRNA	11394
Conflict	38
scRNA	1
operon	1388
transcription_end_site	92672
base_call_error_correction	1553
ambiguous	68
Clone	3544
Possible_frameshift	1
TF_binding_site	884304
Annotation	2
expressed_sequence_match	8134692
miRNA	451
CpG	9
Alu	15
low_complexity_region	189148
similarity	645505
transposable_element	760
transposable_element_CDS	333
Clooe	1
DNAseI_hypersensitive_site	7095
Motif	164476
transposable_element_pseudogene	21
protein_match	3011242
misc_feature	18712
Transcript	188
SAGE_tag	292974
annotation	2815
comment	211
point_mutation	1058051
snoRNA	345
exon	3438394
region	6541
TeamLeader	11
TSS_region	73499
CDS	716562
reagent	519568
SL1_acceptor_site	43207
substitution	9384
Masked	1
asRNA	104
Warning	9
SL2_acceptor_site	13445
resolved	2
Direct	24
stop	7
transposable_element_insertion_site	15888
Pseudogene	2173
Inverted	33
Stolen	8
```

    repeat_types="repeat inverted_repeat tandem_repeat tandem_duplication segmental_duplication repeat_region transposable_element transposable_element_CDS transposable_element_pseudogene"
    cd /Genomes/C_elegans/Wormbase/WS253
    gunzip c_elegans.PRJNA13758.WS253.annotations.gff2.gz
    bioawk -t '$3 == "transposable_element" {print $1,$2,"transcript",$4,$5,$6,$7,$8,$9}' \
        c_elegans.PRJNA13758.WS253.annotations.gff2 \
        | sed 's/^CHROMOSOME_//' \
        | sed 's/Transposon \("WBTransposon........"\)$/gene_id \1; transcript_id \1;/' \
        > ../../repeats/transposable_elements_wormbase.gtf
    ~/src/RNA_Seq_Cecere/ensembl_gtf2bed/ensembl_gtf2bed < ../../repeats/transposable_elements_wormbase.gtf > ../../repeats/transposable_elements_wormbase.bed
    ln -s "../../../../../../../repeats/transposable_elements_wormbase.gtf" ../../Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/transposable_elements_wormbase.gtf
    ln -s "../../../../../../../repeats/transposable_elements_wormbase.bed" ../../Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/transposable_elements_wormbase.bed


Information associated with WBTransposon identifiers might be here:
<ftp://ftp.wormbase.org/pub/wormbase/datasets-wormbase/intermine/WS252/Transposon_family.xml.gz>

    mkdir -p geneIDs/transposons
    wget ftp://ftp.wormbase.org/pub/wormbase/datasets-wormbase/intermine/WS252/Transposon.xml.gz
    zcat Transposon.xml.gz > geneIDs/transposons/Transposon.xml
    rm -f Transposon.xml.gz
    parse_wormbase_transposon_xml.py geneIDs/transposons/Transposon.xml > geneIDs/transposons/cosmid_wormid_transposon_family.txt


Data provided by HISAT2
-----------------------


### UCSC ce10

    cd /Genomes/C_elegans
    mkdir hisat2
    cd hista2
    wget ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat2/data/ce10.tar.gz
    tar -xvzf ce10.tar.gz

### Ensembl WBcel235

    cd /Genomes/C_elegans
    mkdir hisat2
    cd hista2
    wget ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat2/data/wbcel235.tar.gz
    wget ftp://ftp.ccb.jhu.edu/pub/infphilo/hisat2/data/wbcel235_tran.tar.gz
    tar -xvzf wbcel235.tar.gz
    tar -xvzf wbcel235_tran.tar.gz
    rm -f wbcel235.tar.gz
    rm -f wbcel235_tran.tar.gz


Create index for CRAC
---------------------

    cd /Genomes/C_elegans
    mkdir crac
    crac-index index crac/wbcel235 Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa



Coordinate conversion
=====================


    cd /Genomes/C_elegans/Wormbase
    wget ftp://ftp.sanger.ac.uk/pub2/wormbase/software/Remap-between-versions/remap.tar.bz2
    tar -xvjf remap.tar.bz2
    
The files distributed with the script are not documented, and my interpretation of their meaning makes no sense.


I wrote the following to the script author:

-----
Dear Gary Williams,

I see that you wrote a perl script to convert coordinates in gff files
from one C elegans genome version to another.

I would like to write a script converting just one coordinate, and not
an interval, but I fail to find the documentation of the format used in
the files that your script uses:
ftp://ftp.sanger.ac.uk/pub2/wormbase/software/Remap-between-versions/Mapping-data/

By having a look at your script and at the files, I first thought that
the lines in these files represented insertions, deletions or/and flips,
with columns 1 and 2 indicating original "bed-style" coordinates of a
region, columns 4 and 5 indicating the corresponding transformed
coordinates, and column 7 was a "1" if the transformation was a flip.

But then I found a line that reads as follows:
```
11700859	11700860	1	11700859	11700860	1	0
```

(4th line of
ftp://ftp.sanger.ac.uk/pub2/wormbase/software/Remap-between-versions/Mapping-data/sequence_differences.WS96)

Either the file contains an error, either I'm not understanding its
format correctly: It maps the 1-base region 11700859-11700860 to the
same 1-base region.

In the tarballed files distributed along with your script, I actually
find 65 cases where a region seems mapped to itself and is not a flip:

```
cat CHROMOSOME_DIFFERENCES/sequence_differences.WS* | grep -v
"^Chromosome" | awk '($1 == $4) && ($2 == $5) && ($7==0) {print}' | wc -l
65
```


How should I interpret the format of these files ? Is it documented
somewhere ?


Best regards,

Blaise Li

-----

One can note that there is actually no coordinate changes between WS220 and WS234, so we can use <https://genome.ucsc.edu/cgi-bin/hgLiftOver>
to make a WS220/ce10 -> Wcel235/ce11 conversion



Looking for TSS annotations
===========================

TSS is not easy to identify by RNA-seq because 70% of the genes in C. elegans
have trans-splicing: an "outron" is removed at the 5' end.

GRO-cap performed by Kruesi et. al (2013) provides information about TSS.


Annotation of transcription start sites for protein-coding genes:
<https://elifesciences.org/lookup/doi/10.7554/eLife.00808.005>


Annotation of transcription start sites for non-coding RNAs:
<https://elifesciences.org/lookup/doi/10.7554/eLife.00808.006>

    cd /Genome/C_elegans
    mkdir TSS_annotations
    cd TSS_annotations
    wget https://elife-publishing-cdn.s3.amazonaws.com/00808/elife-00808-fig1-data2-v1-download.xls -O Kruesi_TSS_coding.xls
    #wget https://elife-publishing-cdn.s3.amazonaws.com/00808/elife-00808-fig1-data3-v1-download.xls -O Kruesi_TSS_non-coding.xls
    

We opened the file with libreoffice and saved it as `Kruesi_TSS_coding.csv` in tab-separated format.

    bioawk -t 'NR>34 && $9 {print $1"\t"$9-1"\t"$9"\t"$2"|"$3"@"$1":"$5"-"$6"|"$4}' Kruesi_TSS_coding.csv > Kruesi_TSS_coding_DCC_mutant_embryo.bed
    bioawk -t 'NR>34 && $10 {print $1"\t"$10-1"\t"$10"\t"$2"|"$3"@"$1":"$5"-"$6"|"$4}' Kruesi_TSS_coding.csv > Kruesi_TSS_coding_WT_embryo.bed
    bioawk -t 'NR>34 && $11 {print $1"\t"$11-1"\t"$11"\t"$2"|"$3"@"$1":"$5"-"$6"|"$4}' Kruesi_TSS_coding.csv > Kruesi_TSS_coding_WT_starved_L1.bed
    bioawk -t 'NR>34 && $12 {print $1"\t"$12-1"\t"$12"\t"$2"|"$3"@"$1":"$5"-"$6"|"$4}' Kruesi_TSS_coding.csv > Kruesi_TSS_coding_WT_L3.bed


Converting coordinates
----------------------

    # see http://hgdownload.cse.ucsc.edu/goldenPath/ce10/liftOver/
    wget http://hgdownload.cse.ucsc.edu/goldenPath/ce10/liftOver/ce10ToCe11.over.chain.gz
    wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/liftOver
    chmod +x liftOver
    for stage in "DCC_mutant_embryo" "WT_embryo" "WT_starved_L1" "WT_L3"
    do
        oldfile="Kruesi_TSS_coding_${stage}.bed"
        newfile="Kruesi_TSS_coding_${stage}_ce11.bed"
        unmapped="Kruesi_TSS_coding_${stage}_unmapped.txt"
        ./liftOver ${oldfile} ce10ToCe11.over.chain.gz ${newfile} ${unmapped}
        sorted="Kruesi_TSS_coding_${stage}_ce11_sorted.bed"
        sort -k1,1 -k2,2n ${newfile} > ${sorted}
    done


ModENCODE
=========
    
    #cd /Genomes/C_elegans
    #mkdir modENCODE
    #cd modENCODE
    #wget --continue "ftp://data.modencode.org/C.elegans/mRNA/integrated-gene-model/gene-model_gff3/transfrag%3ADevelopmental-Stage=Young-adult-(pre-gravid)-46-hrs-post-L1-stage-larvae%23Strain=N2%3Aintegrated-gene-model%3ARep-1%3A%3ACele_WS220%3AmodENCODE_2961.gff3.gz"


ERCC spike-ins
==============

    cd /pasteur/entites/Mhe/Genomes
    mkdir -p spike-ins
    cd spike-ins
    wget https://assets.thermofisher.com/TFS-Assets/LSG/manuals/ERCC92.zip
    unzip ERCC92.zip
    #mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WithERCC92
    #cat /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa ERCC92.fa \
    #    > /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WithERCC92

Making genome indices
---------------------

    genome_fa="/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa"
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_ERCC92
    hisat2-build ${genome_fa},ERCC92.fa /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_ERCC92/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_ERCC92
    bowtie2-build --seed 123 --packed ${genome_fa},ERCC92.fa /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_ERCC92/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_ERCC92
    crac-index index /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_ERCC92/wbcel235_ERCC92 ${genome_fa} ERCC92.fa

Adding a `spike_ins` "biotype" to the annotations
-------------------------------------------------

    cd /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes
    cat /pasteur/entites/Mhe/Genomes/spike-ins/ERCC92.gtf \
        | sed 's/exon/transcript/' | sed 's/;$/; gene_biotype "spike_ins";/' \
        > spike_ins.gtf
    compute_genes_exon_lengths.py --annot_dir "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes"


mCherry transgene
=================

    cd /pasteur/entites/Mhe/Genomes
    mkdir -p transgenes
    cd transgenes
    # mCherry transgene structure is stored in the make_mCherry_transgene.py script
    # It creates:
    # - mCherry_transgene_parts.fa
    # - mCherry_transcript.fa (joined exons and introns)
    # - mCherry.gtf (describes the above transcript as covering one single extra chromosome)
    # - mCherry_transgene.gtf
    make_mCherry_transgene.py
    cp mCherry.gtf /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/.
    cat /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf mCherry.gtf /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/*_transposons_rmsk.gtf > /pasteur/entites/Mhe/Genomes/C_elegans/WBcel235_mCherry_genes_transposons.gtf
    compute_genes_exon_lengths.py --annot_dir "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes"
    cp /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed \
        /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed
    transcript_len=$(awk '$3 == "transcript" {print $5}' mCherry.gtf)
    echo -e "mCherry\t0\t${transcript_len}" \
        | bedops --chop 10 - \
        >> /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed


Making genome indices
---------------------

    cd /pasteur/entites/Mhe/Genomes/transgenes
    genome_fa="/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa"
    cat ${genome_fa} mCherry_transcript.fa > /pasteur/entites/Mhe/Genomes/C_elegans/WBcel235_mCherry.fa
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_mCherry
    hisat2-build ${genome_fa},mCherry_transcript.fa /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_mCherry/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_mCherry
    bowtie2-build --seed 123 --packed ${genome_fa},mCherry_transcript.fa /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_mCherry/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_mCherry
    crac-index index /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_mCherry/wbcel235_mCherry ${genome_fa} mCherry_transcript.fa


Codon-optimized klp-7
=====================

    cd /pasteur/entites/Mhe/Genomes
    mkdir -p transgenes
    cd transgenes
    # klp7co transgene structure is stored in the make_klp7co_transgene.py script
    # It creates:
    # - klp7co_transgene_parts.fa
    # - klp7co_transcript.fa (joined exons and introns)
    # - klp7co.gtf (describes the above transcript as covering one single extra chromosome)
    # - klp7co_transgene.gtf
    make_klp7co_transgene.py
    cp klp7co.gtf /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/.
    cat /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes.gtf klp7co.gtf /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/*_transposons_rmsk.gtf > /pasteur/entites/Mhe/Genomes/C_elegans/WBcel235_klp7co_genes_transposons.gtf
    compute_genes_exon_lengths.py --annot_dir "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes"
    cp /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed \
        /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_klp7co_binned_10.bed
    transcript_len=$(awk '$3 == "transcript" {print $5}' klp7co.gtf)
    echo -e "klp7co\t0\t${transcript_len}" \
        | bedops --chop 10 - \
        >> /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_klp7co_binned_10.bed


Making genome indices
---------------------

    cd /pasteur/entites/Mhe/Genomes/transgenes
    genome_fa="/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.fa"
    cat ${genome_fa} klp7co_transcript.fa > /pasteur/entites/Mhe/Genomes/C_elegans/WBcel235_klp7co.fa
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_klp7co
    hisat2-build ${genome_fa},klp7co_transcript.fa /pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_klp7co/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_klp7co
    bowtie2-build --seed 123 --packed ${genome_fa},klp7co_transcript.fa /pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_klp7co/genome
    mkdir -p /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_klp7co
    crac-index index /pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_klp7co/wbcel235_klp7co ${genome_fa} klp7co_transcript.fa



Testing a workflow to set up genomic data
=========================================

It uses the singularity container of the data analysis workflows.

    cd /pasteur/homes/bli/src/10825_Cecere/Genomes
    # configs/WBcel235_mCherry_genome.yaml contains a reference to
    # /pasteur/homes/bli/src/10825_Cecere/Genomes/scripts/make_transgene.py
    # and
    # /pasteur/homes/bli/src/10825_Cecere/Genomes/configs/WBcel235_mCherry_transgene.yaml
    snakemake --snakefile workflow/format_genome.snakefile --configfile configs/WBcel235_mCherry_genome.yaml -j 34
    mv WBcel235_mCherry_19122019 /pasteur/entites/Mhe/Genomes/.

Now `"/pasteur/entites/Mhe/Genomes/WBcel235_mCherry_19122019/genome_dict.yaml"`
can be provided for the `genome_dict` section of an analysis pipeline
configuration.

