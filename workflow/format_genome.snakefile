# Copyright (C) 2020-2022 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Snakefile to prepare genomic data.

This creates a folder where the desired genomic data is downloaded and
formatted, and the folder includes a `genome_dict` configuration file to insert
in data analysis pipeline configuration.


TODO: Generate an archive of all this? An installer?

Example genome dict (not outputted by the present pipeline, but used in the configuration of an analysis pipeline):

    genome_dict:
    {
        "name" : "C_elegans_mCherry",
        "size" : "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/GenomeSize.xml",
        # If the above file does not contain size information for some of the chromosomes, add it here:
        "extra_chromosomes" : {"mCherry" : "897"},
        "db" : {
            "bowtie2" : "/pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_mCherry/genome",
            "crac" : "/pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_mCherry/wbcel235_mCherry",
            "hisat2" : "/pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_mCherry/genome"},
        # Generation of these files is described in README_Genomes.md
        # cp /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed \
        #     /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed
        # echo -e "mCherry\t0\t897" \
        #     | bedops --chop 10 - \
        #     >> /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed
        "binned" : "/pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed",
        "annot_dir" : "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes",
        "convert_dir" : "/pasteur/entites/Mhe/Genomes/C_elegans/Wormbase/WS253/geneIDs",
        "converter": "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes/genes_id2name.pickle",
        "gene_lists_dir" : "/pasteur/entites/Mhe/Gene_lists",
    },

"""
import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")
import os
from pathlib import Path
import shutil
from yaml import dump as ydump
import pandas as pd
from mappy import fastx_read
from cytoolz import valmap
from libhts import (
    gtf_2_genes_exon_lengths,
    repeat_bed_2_lengths,
    # TODO: Add possibility to include spike-ins in the genome.
    #spikein_gtf_2_lengths,
    )

simg = config.get("simg", "/opt/bioinfo_utils/bin/run_pipeline")

genus = config.get("genus", "Caenorhabditis")
species = config.get("species", "elegans")
genome_version = config.get("genome_version", "WBcel235")
genome_code = config.get("genome_code", "ce11")
genome_name = config.get(
    "genome_name",
    f"{genus[0]}_{species}_{genome_version}")

install_dir = Path(config.get("install_dir", "/pasteur/entites/Mhe/Genomes"))
# Should not be a Path object, in order to avoid `re_path`:
gene_lists_dir = config.get("gene_lists_dir", "/pasteur/entites/Mhe/Gene_lists")
codon_goodness_table = config.get("codon_goodness_table", "/pasteur/entites/Mhe/Genomes/C_elegans/codon_goodness.tsv")
# The goal is to obtain a folder containing everything,
# and that is to be installed in install_dir.
# The paths in the genome_dict will have to be set relative to install_dir before dumping.
genome_dir = Path(config["genome_dir"])
annot_dir = genome_dir.joinpath("Annotation", "Archives", "archive-current", "Genes")
final_annot_dir = genome_dir.joinpath("Annotation", "With_transgenes")
repeats_dir = genome_dir.joinpath("repeats")
transgenes_dir = genome_dir.joinpath("transgenes")
binned_bed = genome_dir.joinpath("genome_binned_10.bed")
sequence_dir = genome_dir.joinpath("Sequence", "WholeGenomeFasta")
genome_fa = sequence_dir.joinpath("genome.fa")
genome_sam_head = sequence_dir.joinpath("genome.dict")
# TODO: give directory containing either fasta and gtf file
# or scripts to generate
# for each transgene: a transgene name and a path to a script that generates fasta sequences and annotations
transgenes = config.get("transgenes", {})
transgene_biotypes = list({
    info["biotype"] for info in transgenes.values()})
transgenes_fa = [
    # sequence_dir.joinpath(f"{transgene}.fa")
    transgenes_dir.joinpath(f"{transgene}_transcript.fa")
    for transgene in transgenes.keys()]
transgene_annots = [
    # annot_dir.joinpath(f"{transgene}.gtf")
    transgenes_dir.joinpath(f"{transgene}.gtf")
    for transgene in transgenes.keys()]
size_file = sequence_dir.joinpath("GenomeSize.xml")
mapper_index_dirs = {
    "bowtie2": genome_dir.joinpath("bowtie2"),
    "hisat2": genome_dir.joinpath("hisat2"),
}


BASE_BIOTYPES = "antisense pseudogene snRNA lincRNA tRNA piRNA protein_coding rRNA miRNA snoRNA ncRNA all".split()
PROT_SUBBIOTYPES = [
    "protein_coding_CDS",
    "protein_coding_UTR",
    "protein_coding_CDS_UTR",
    "protein_coding_pure_intron",
    "protein_coding_5UTR",
    "protein_coding_3UTR"]
REPEAT_BIOTYPES = [
    "transposable_elements_rmsk",
    "DNA_transposons_rmsk",
    "RNA_transposons_rmsk",
    "satellites_rmsk",
    "simple_repeats_rmsk"]
ALL_BIOTYPES = list({biotype for biotype in BASE_BIOTYPES + PROT_SUBBIOTYPES + REPEAT_BIOTYPES + transgene_biotypes})
TRANSGENE_ONLY_BIOTYPES = list(set(transgene_biotypes) - {biotype for biotype in BASE_BIOTYPES + PROT_SUBBIOTYPES + REPEAT_BIOTYPES})

final_annots = [
    final_annot_dir.joinpath(f"{biotype}.gtf")
    for biotype in ALL_BIOTYPES]
final_beds = [
    final_annot_dir.joinpath(f"{biotype}.bed")
    for biotype in ALL_BIOTYPES]
transgene_beds = [
    final_annot_dir.joinpath(f"{transgene}.bed")
    for transgene in transgenes.keys()]
# TODO: Would need to be explicitly generated if a transgene was not associated with among of BASE_BIOTYPES or REPEAT_BIOTYPES
#final_annots.expand([
#    final_annot_dir.joinpath(f"{transgene}.gtf")
#    for transgene in transgenes.keys()])

genome_dict = {
    "name": genome_name,
    # TODO: fix paths
    "size": size_file,
    # If the above file does not contain size information for some of the chromosomes, add it here:
    # "extra_chromosomes" : {"mCherry" : "897"},
    # Added in make_genome_dict
    # "extra_chromosomes": extra_chromosomes,
    # "db" : {
    #     "bowtie2" : "/pasteur/entites/Mhe/Genomes/C_elegans/bowtie2/WBcel235_mCherry/genome",
    #     "crac" : "/pasteur/entites/Mhe/Genomes/C_elegans/crac/WBcel235_mCherry/wbcel235_mCherry",
    #     "hisat2" : "/pasteur/entites/Mhe/Genomes/C_elegans/hisat2/WBcel235_mCherry/genome"},
    "db": {
        mapper: index_dir.joinpath("genome")
        for (mapper, index_dir)
        in mapper_index_dirs.items()},
    # Generation of these files is described in README_Genomes.md
    # cp /pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed \
    #     /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed
    # echo -e "mCherry\t0\t897" \
    #     | bedops --chop 10 - \
    #     >> /pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed
    # "binned" : "/pasteur/entites/Mhe/Genomes/transgenes/WBcel235_mCherry_binned_10.bed",
    "binned": binned_bed,
    # "annot_dir" : "/pasteur/entites/Mhe/Genomes/C_elegans/Caenorhabditis_elegans/Ensembl/WBcel235/Annotation/Genes",
    "annot_dir": final_annot_dir,
    # "convert_dir" : "/pasteur/entites/Mhe/Genomes/C_elegans/Wormbase/WS253/geneIDs",
    "converter": final_annot_dir.joinpath("genes_id2name.pickle"),
    "gene_lists_dir" : gene_lists_dir,
    "codon_goodness_table": codon_goodness_table,}


def re_path(genome_dict):
    """
    Recursively convert paths in *genome_dict* so that they are correct once
    genome_dir is made a sub-folder of install_dir.
    """
    try:
        # If this is a Path object:
        return str(install_dir.joinpath(genome_dir.name, genome_dict.relative_to(genome_dir)))
    except ValueError:
        # The path is not in genome_dir
        return str(genome_dict)
    except AttributeError:
        # https://stackoverflow.com/a/62430764/1878788
        try:
            # If this is a dictionary
            return valmap(re_path, genome_dict)
        except (AttributeError, TypeError):
            # This is likely something else, likely a string or a number
            return str(genome_dict)

rule all:
    input:
        size_file,
        binned_bed,
        final_beds,
        transgene_beds,
        final_annot_dir.joinpath("union_exon_lengths.txt"),
        mapper_index_dirs["bowtie2"],
        mapper_index_dirs["hisat2"],
        genome_dir.joinpath("genome_dict.yaml"),


rule download_genome:
    """Get genomic data from igenome, extract gtf files for each biotype and make corresponding bed file."""
    output:
        # annot_dir.joinpath("genes.gtf"),
        annot_dir.joinpath(".downloaded_done"),
        size_file,
        genome_sam_head,
        genome_fa,
    params:
        # Credentials no longer work (22/12/2020)
        # igenome_base_url = "ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com",
        igenome_base_url = "http://igenomes.illumina.com.s3-website-us-east-1.amazonaws.com",
    singularity:
        simg
    shell:
        f"""
        wget {{params.igenome_base_url}}/{genus}_{species}/Ensembl/{genome_version}/{genus}_{species}_Ensembl_{genome_version}.tar.gz
        tar -xvzf {genus}_{species}_Ensembl_{genome_version}.tar.gz
        rm -rf {genus}_{species}_Ensembl_{genome_version}.tar.gz
        # To avoid mv from putting things in a subdirectory
        rm -rf {genome_dir}
        mv {genus}_{species}/Ensembl/{genome_version} {genome_dir}
        # mv {genus}_{species}/Ensembl/{genome_version}/* {genome_dir}/.
        touch {annot_dir}/.downloaded_done
        # ln -s genes.gtf {annot_dir}/genes_all.gtf
        """


# rule make_genes_gtf_link:
#     input:
#         genome_dir,
#     output:
#         directory(annot_dir),
#     singularity:
#         simg
#     shell:
#         """
#         ln -s genes.gtf {{output[0]}}/genes_all.gtf
#         """


# rule just_check_files:
#     input:
#         genome_dir,
#     output:
#         genome_fa,
#         size_file,
#     singularity:
#         simg
#     shell:
#         """
#         [ -e {output[0]} ] && touch {output[0]}
#         [ -e {output[1]} ] && touch {output[1]}
#         """


rule get_rmsk:
    """Get repeated elements information from hgdownload."""
    input:
        #genome_dir,
        #annot_dir,
        # Used to ensure genome_dir has been created
        # annot_dir.joinpath("genes.gtf"),
        annot_dir.joinpath(".downloaded_done"),
    output:
        # directory(repeats_dir),
        repeats_dir.joinpath(".rmsk_done"),
    singularity:
        simg
    # TODO: Check whether bed versions could be generated using gtf2bed
    shell:
        f"""
        mkdir -p {repeats_dir}
        cd {repeats_dir}
        wget http://hgdownload.cse.ucsc.edu/goldenPath/{genome_code}/database/rmsk.txt.gz
        gunzip rmsk.txt.gz
        bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "DNA" || $12 == "DNA?" || $12 == "Unknown" {{{{count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \\"transposable_elements_rmsk\\"; gene_id \\""$12"|"$13"|"$11":"count[$11]"\\"; transcript_id \\""$12"|"$13"|"$11":"count[$11]"\\";"}}}}' rmsk.txt \\
            | sed 's/^chr//' > transposable_elements.gtf
        bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "DNA" || $12 == "DNA?" || $12 == "Unknown" {{{{count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}}}}' \\
            rmsk.txt | sed 's/^chr//' > transposable_elements.bed
        # DNA transposons
        bioawk -t '$12 == "RC" || $12 == "DNA" || $12 == "DNA?" {{{{count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \\"DNA_transposons_rmsk\\"; gene_id \\""$12"|"$13"|"$11":"count[$11]"\\"; transcript_id \\""$12"|"$13"|"$11":"count[$11]"\\";"}}}}' rmsk.txt \\
            | sed 's/^chr//' > DNA_transposons.gtf
        bioawk -t '$12 == "RC" || $12 == "DNA" || $12 == "DNA?" {{{{count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}}}}' \\
            rmsk.txt | sed 's/^chr//' > DNA_transposons.bed
        # RNA transposons
        bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "SINE" {{{{count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \\"RNA_transposons_rmsk\\"; gene_id \\""$12"|"$13"|"$11":"count[$11]"\\"; transcript_id \\""$12"|"$13"|"$11":"count[$11]"\\";"}}}}' rmsk.txt \\
            | sed 's/^chr//' > RNA_transposons.gtf
        bioawk -t '$12 == "LINE" || $12 == "LTR" || $12 == "SINE" {{{{count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}}}}' \\
            rmsk.txt | sed 's/^chr//' > RNA_transposons.bed
        # Satellites
        bioawk -t '$12 == "Satellite" {{{{count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \\"satellites_rmsk\\"; gene_id \\""$12"|"$13"|"$11":"count[$11]"\\"; transcript_id \\""$12"|"$13"|"$11":"count[$11]"\\";"}}}}' rmsk.txt \\
            | sed 's/^chr//' > satellites.gtf
        bioawk -t '$12 == "Satellite" {{{{count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}}}}' \\
            rmsk.txt | sed 's/^chr//' > satellites.bed
        # Simple repeats
        bioawk -t '$12 == "Simple_repeat" {{{{count[$11]++; print $6,"rmsk","transcript",$7+1,$8+1,".",$10,".","gene_biotype \\"simple_repeats_rmsk\\"; gene_id \\""$12"|"$13"|"$11":"count[$11]"\\"; transcript_id \\""$12"|"$13"|"$11":"count[$11]"\\";"}}}}' rmsk.txt \\
            | sed 's/^chr//' > simple_repeats.gtf
        bioawk -t '$12 == "Simple_repeat" {{{{count[$11]++; print $6,$7,$8+1,$12"|"$13"|"$11":"count[$11],".",$10}}}}' \\
            rmsk.txt | sed 's/^chr//' > simple_repeats.bed
        touch .rmsk_done
        """


# TODO: make a script to generate spike-in .fa and .gtf so as to be used like transgenes.
rule make_transgene_info:
    """Generate information pertaining to transgenes."""
    input:
        #genome_dir,
        # Used to ensure genome_dir has been created
        # annot_dir.joinpath("genes.gtf"),
        annot_dir.joinpath(".downloaded_done"),
    output:
        transgene_fa = transgenes_dir.joinpath("{transgene}_transcript.fa"),
        transgene_annot = transgenes_dir.joinpath("{transgene}.gtf"),
    params:
        transgene_script = lambda wildcards: transgenes[wildcards.transgene]["script"],
        # transgene_biotype = lambda wildcards: transgenes[wildcards.transgene]["biotype"],
        transgene_config = lambda wildcards: transgenes[wildcards.transgene]["config"],
    singularity:
        simg
    shell:
        """
        mkdir -p {transgenes_dir}
        {params.transgene_script} -d {transgenes_dir} -c {params.transgene_config}
        """


rule extract_gtf_per_biotype:
    """Make a gtf file for each biotype."""
    input:
        #genome_dir,
        #annot_dir,
        # repeats_dir,
        # Used to ensure repeats_dir has been created
        repeats_dir.joinpath(".rmsk_done"),
        # Used to ensure genome_dir has been created
        # annot_dir.joinpath("genes.gtf"),
        annot_dir.joinpath(".downloaded_done"),
    output:
        # Created by extract_info_from_gtf.py
        id2name = annot_dir.joinpath("genes_id2name.pickle"),
        genes_all = annot_dir.joinpath("genes_all.gtf"),
        gtf_out = [annot_dir.joinpath(f"{biotype}.gtf") for biotype in ALL_BIOTYPES],
    params:
        extra_opts = config.get("rules_extra_params", {}).get("extract_gtf_per_biotype", "")
# Missing input files for rule gather_annotations:
# /pasteur/homes/pquarato/Documents/create_transgene_genome_bli_script/WBcel235_mCherry_Rechavi_Cell_2020/Annotation/With_transgenes/protein_coding_UTR.gtf
# /pasteur/homes/pquarato/Documents/create_transgene_genome_bli_script/WBcel235_mCherry_Rechavi_Cell_2020/Annotation/With_transgenes/protein_coding_pure_intron.gtf
# /pasteur/homes/pquarato/Documents/create_transgene_genome_bli_script/WBcel235_mCherry_Rechavi_Cell_2020/Annotation/With_transgenes/protein_coding_CDS.gtf
    log:
        log = genome_dir.joinpath("logs", "extract_gtf_per_biotype.log")
    # singularity:
    #     simg
    run:
        os.symlink("genes.gtf", output.genes_all)
        genes_gtf = annot_dir.joinpath("genes.gtf")
        # Generating separate gtf files genes_{biotype}.gtf
        # Issue: biotypes hard-coded in the script and the gtf file
        shell(f"extract_info_from_gtf.py -g {str(genes_gtf)} {params.extra_opts} >> {log.log}")
        for biotype in BASE_BIOTYPES:
            #in_gtf = annot_dir.joinpath(f"genes_{biotype}.gtf")
            #out_bed = annot_dir.joinpath("genes_{biotype}.bed")
            # gtf2bed is a Haskell program available in https://gitlab.pasteur.fr/bli/bioinfo_utils.git
            # shell(f"gtf2bed < {str(in_gtf)} | sort -k1,1 -k4,4 -k2,2n | uniq | bedtools groupby -g 1,4,5,6 -c 2,3 -o min,max | awk -v OFS="\\t" '{{{{print $1,$5,$6,$2,$3,$4}}}}' | sort -k1,1 -k2,2n -k3,3n > {str(out_bed)}")
            os.symlink(f"genes_{biotype}.gtf", annot_dir.joinpath(f"{biotype}.gtf"))
        for biotype in REPEAT_BIOTYPES:
            src = os.path.relpath(
                repeats_dir.joinpath(f"{biotype[:-5]}.gtf"),
                annot_dir)
            os.symlink(src, annot_dir.joinpath(f"{biotype}.gtf"))
        for biotype in TRANSGENE_ONLY_BIOTYPES:
            # Those gtfs will be populated later
            annot_dir.joinpath(f"{biotype}.gtf").touch()
        # Extracting exons and generating "pure introns"
        #################################################

        # We assume that "CDS" and "UTR" together form the protein-coding gene
        # exon annotations.
        # Note: We use "transcript" as gtf feature (column 3) in order to simplify the
        # counting by featureCounts, but this results in the annotations not displaying
        # correctly in IGV, probably because several "transcripts" end up having the same
        # `transcript_id`.
        cds_gtf = annot_dir.joinpath(f"protein_coding_CDS.gtf")
        shell(f"""
bioawk -t '$3 == "CDS" {{{{$3="transcript"; print}}}}' {str(genes_gtf)} \
    | sed 's/gene_biotype \\"protein_coding\\"/gene_biotype \\"protein_coding_CDS\\"/' \\
    > {str(cds_gtf)}
        """)
        utr_gtf = annot_dir.joinpath(f"protein_coding_UTR.gtf")
        shell(f"""
bioawk -t '$3 == "UTR" {{{{$3="transcript"; print}}}}' {str(genes_gtf)} \
    | sed 's/gene_biotype \\"protein_coding\\"/gene_biotype \\"protein_coding_UTR\\"/' \\
    > {str(utr_gtf)}
        """)
        # Merge the two to get the positions where protein coding exons are present
        cds_utr_gtf = annot_dir.joinpath(f"protein_coding_CDS_UTR.gtf")
        shell(f"""
sort -k1,1 -k4,4n -m \\
    {str(cds_gtf)} \\
    {str(utr_gtf)} \\
    > {str(cds_utr_gtf)}
        """)
        # Substract the exons from the transcripts to get the positions were only introns are present
        protein_gtf = annot_dir.joinpath(f"protein_coding.gtf")
        pure_intron_gtf = annot_dir.joinpath(f"protein_coding_pure_intron.gtf")
        shell(f"""
bedtools subtract \\
    -a {str(protein_gtf)} \\
    -b {str(cds_utr_gtf)} \\
    | sed 's/gene_biotype \\"protein_coding\\"/gene_biotype \"protein_coding_pure_intron\\"/' \\
    | sort -k1,1 -k4,4n > {str(pure_intron_gtf)}
        """)
        # Extract 5'UTR and 5'UTR separately
        shell(f"orient_gtf_UTRs.py --annot_dir {str(annot_dir)}")


def get_transgene_annots(wildcards):
    transgene_annots = []
    for (transgene, info) in transgenes.items():
        if info["biotype"] == wildcards.biotype:
            transgene_annots.append(
                str(transgenes_dir.joinpath(f"{transgene}.gtf")))
    return transgene_annots


rule complement_gtfs_with_transgenes:
    """"""
    input:
        transgene_annots = get_transgene_annots,
        extract_gtf_per_biotype_done = annot_dir.joinpath("genes_id2name.pickle"),
    output:
        final_annot = final_annot_dir.joinpath("{biotype}.gtf"),
    #singularity:
    #    simg
    run:
        transgene_gtfs_args = " ".join(input.transgene_annots)
        shell(f"""
mkdir -p {final_annot_dir}
for transgene_gtf in {transgene_gtfs_args}
do
    cp ${{{{transgene_gtf}}}} {final_annot_dir}/.
done
cat {annot_dir}/{wildcards.biotype}.gtf {transgene_gtfs_args} > {output.final_annot}
""")
        shutil.copy(input.extract_gtf_per_biotype_done, final_annot_dir)


rule generate_bed_files:
    input:
        # Used to ensure repeats_dir has been created
        repeats_dir.joinpath(".rmsk_done"),
        final_annots,
    output:
        final_beds,
        transgene_beds,
        exon_lengths = final_annot_dir.joinpath("union_exon_lengths.txt"),
    run:
        shutil.copy(annot_dir.joinpath("genes.gtf"), final_annot_dir)
        exon_lengths_tables = {
            "genes": gtf_2_genes_exon_lengths(
                final_annot_dir.joinpath("genes.gtf"))}
        # No need to compute exon lengths for BASE_BIOTYPES:
        # This is included in genes.gtf
        for biotype in BASE_BIOTYPES + PROT_SUBBIOTYPES:
            in_gtf = final_annot_dir.joinpath(f"{biotype}.gtf")
            out_bed = final_annot_dir.joinpath("{biotype}.bed")
            # gtf2bed is a Haskell program available in https://gitlab.pasteur.fr/bli/bioinfo_utils.git
            # Merging bed coordinates of variants: https://bioinformatics.stackexchange.com/q/2265/292
            shell(f"""
gtf2bed < {str(in_gtf)} | sort -k1,1 -k4,4 -k2,2n | uniq \\
    | bedtools groupby -g 1,4,5,6 -c 2,3 -o min,max \\
    | awk -v OFS="\\t" '{{{{print $1,$5,$6,$2,$3,$4}}}}' \\
    | sort -k1,1 -k2,2n -k3,3n \\
    > {str(out_bed)}""")
            # os.symlink(f"genes_{biotype}.bed", annot_dir.joinpath(f"{biotype}.bed"))
        for biotype in REPEAT_BIOTYPES:
            src_bed = repeats_dir.joinpath(f"{biotype[:-5]}.bed")
            dest_bed = final_annot_dir.joinpath(f"{biotype}.bed")
            shutil.copy(src_bed, dest_bed)
            if biotype != "transposable_elements_rmsk":
                exon_lengths_tables[biotype] = repeat_bed_2_lengths(
                    dest_bed)
        for biotype in TRANSGENE_ONLY_BIOTYPES:
            # To be filled later
            exon_lengths_tables[biotype] = []
        for transgene in transgenes.keys():
            in_gtf = final_annot_dir.joinpath(f"{transgene}.gtf")
            biotype = transgenes[transgene]["biotype"]
            if biotype == "spike_ins":
                print(f"transgene: {transgene}")
                exon_lengths_tables[transgene] = gtf_2_genes_exon_lengths(
                    in_gtf, direct_len=True)
                print("exon_lengths")
                print(exon_lengths_tables[transgene])
            else:
                exon_lengths_tables[transgene] = gtf_2_genes_exon_lengths(
                    in_gtf)
            out_bed = final_annot_dir.joinpath(f"{transgene}.bed")
            # gtf2bed is a Haskell program available in https://gitlab.pasteur.fr/bli/bioinfo_utils.git
            # Merging bed coordinates of variants: https://bioinformatics.stackexchange.com/q/2265/292
            shell(f"""
gtf2bed < {str(in_gtf)} | sort -k1,1 -k4,4 -k2,2n | uniq \\
    | bedtools groupby -g 1,4,5,6 -c 2,3 -o min,max \\
    | awk -v OFS="\\t" '{{{{print $1,$5,$6,$2,$3,$4}}}}' \\
    | sort -k1,1 -k2,2n -k3,3n \\
    > {str(out_bed)}""")
            if biotype in TRANSGENE_ONLY_BIOTYPES:
                exon_lengths_tables[biotype].append(exon_lengths_tables[transgene])
                out_biotype_bed = final_annot_dir.joinpath(f"{biotype}.bed")
                shell(f"cat {str(out_bed)} >> {out_biotype_bed}")
        # Needed by compute_genes_exon_lengths.py
        # shutil.copy(annot_dir.joinpath("genes.gtf"), final_annot_dir)
        # Pyton script available in https://gitlab.pasteur.fr/bli/bioinfo_utils.git
        # Issue: the script has some hard-coded assumptions
        # about what files to expect in annot_dir
        # shell(f"compute_genes_exon_lengths.py --annot_dir {final_annot_dir}")
        for biotype in TRANSGENE_ONLY_BIOTYPES:
            exon_lengths_tables[biotype] = pd.concat(exon_lengths_tables[biotype])
            print(f"exon_lengths_tables[{biotype}].shape", exon_lengths_tables[biotype].shape)
            exon_lengths_tables[biotype].to_csv(
                final_annot_dir.joinpath(f"{biotype}_union_exon_lengths.txt"), sep="\t")
        # TODO: Understand why there are duplicates
        exon_lengths_table = pd.concat(exon_lengths_tables.values())#.drop_duplicates()
        print("exon_lengths_table.shape", exon_lengths_table.shape)
        exon_lengths_table.to_csv(
            final_annot_dir.joinpath("union_exon_lengths_with_dups.txt"), sep="\t"),
        exon_lengths_table.reset_index().drop_duplicates().set_index("gene").to_csv(
            output.exon_lengths, sep="\t")


# TODO: add bed sections for spike-ins? Treat them like transgenes?
rule make_binned_bed:
    """Make a bed file defining 10bp bins along the genome."""
    input:
        genome_sam_head,
        # Necessary to have the genome_fa
        # genome_dir,
        # genome and extra "transgenes"
        genome_fa,
        transgene_beds,
    output:
        bed = binned_bed,
    # singularity:
    #     simg
    run:
        shell(f"""
mawk '$1 == "@SQ" {{{{print $2"\\t0\\t"$3}}}}' {genome_sam_head} \\
    | sed 's/SN://g' | sed 's/LN://' \\
    | bedops --chop 10 - \\
    > {output.bed}
""")
        for transgene_bed in transgene_beds:
            shell(f"cat {transgene_bed} | bedops --chop 10 - >> {output.bed}")


# TODO: Either add spike-ins here or include them as transgenes
def generate_comma_separated_fastas(wildcards, input):
    return ",".join([input.genome_fa, *input.transgenes_fa])


rule make_bowtie2_index:
    input:
        # genome and extra "transgenes"
        genome_fa = genome_fa,
        transgenes_fa = transgenes_fa,
    output:
        index_dir = directory(mapper_index_dirs["bowtie2"])
    params:
        # fasta=lambda wildcards, input: ",".join(input[0], *input[1])
        fasta = generate_comma_separated_fastas
    singularity:
        simg
    shell:
        """
        mkdir -p {output[0]}
        bowtie2-build --seed 123 --packed {params.fasta} {output[0]}/genome
        """


rule make_hisat2_index:
    input:
        # genome and extra "transgenes"
        genome_fa = genome_fa,
        transgenes_fa = transgenes_fa,
    output:
        index_dir = directory(mapper_index_dirs["hisat2"])
    params:
        # fasta=lambda wildcards, input: ",".join(input[0], *input[1])
        fasta = generate_comma_separated_fastas
    singularity:
        simg
    shell:
        """
        mkdir -p {output[0]}
        hisat2-build --seed 123 {params.fasta} {output[0]}/genome
        """


rule make_genome_dict:
    input:
        # any file that ensures the genome has been downloaded
        size_file,
        # Using directories as inputs and outputs doesn't work well
        # genome_dir,
        transgenes_fa,
        final_annots,
        final_beds,
        binned_bed,
        final_annot_dir.joinpath("union_exon_lengths.txt"),
    output:
        genome_dir.joinpath("genome_dict.yaml"),
    run:
        # Establish extra chromosome lenghts
        extra_chromosomes = {}
        for transgene_fa in transgenes_fa:
            for (name, seq, *_) in fastx_read(str(transgene_fa)):
                if name in extra_chromosomes:
                    raise ValueError(f"Name conflict for transgene pseudo-chromosomes: {name}")
                extra_chromosomes[name] = len(seq)
        # Set extra_chromosomes
        genome_dict["extra_chromosomes"] = extra_chromosomes
        with open(output[0], "w") as fh:
            ydump(re_path(genome_dict), fh)


onsuccess:
    print(f"Genome preparation finished. You can now move {genome_dir} to {install_dir}")
    print(f"For the configuration of your pipelines, you can use {genome_dir.joinpath('genome_dict.yaml')} as `genome_dict`")


onerror:
    #shell(f"rm -rf {genome_dir}_err")
    #shell(f"cp -rp {genome_dir} {genome_dir}_err")
    print("Genome preparation failed.")

