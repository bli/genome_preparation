Genome preparation workflow
===========================

This repository contains a genome preparation workflow that should be used
before configuring and running high-throughput sequencing data analysis
workflows found in <https://gitlab.pasteur.fr/bli/bioinfo_utils>.


Example usage
-------------

This may not be up-to-date. In particular paths in the exemple and inside
the configuration files should be modified in order to match the local
directory structure.

In order to ensure all dependencies are met, the workflow could be run
using the singularity container of the data analysis workflows (see
<https://gitlab.pasteur.fr/bli/bioinfo_utils/-/tree/master/singularity>).

    cd /pasteur/homes/bli/src/10825_Cecere/Genomes
    # configs/WBcel235_mCherry_genome.yaml contains a reference to
    # /pasteur/homes/bli/src/10825_Cecere/Genomes/scripts/make_transgene.py
    # and
    # /pasteur/homes/bli/src/10825_Cecere/Genomes/configs/WBcel235_mCherry_transgene.yaml
    snakemake --snakefile workflow/format_genome.snakefile --configfile configs/WBcel235_mCherry_genome.yaml -j 34
    mv WBcel235_mCherry_19122019 /pasteur/entites/Mhe/Genomes/.

Now `"/pasteur/entites/Mhe/Genomes/WBcel235_mCherry_19122019/genome_dict.yaml"`
can be provided for the `genome_dict` section of an analysis pipeline
configuration.

